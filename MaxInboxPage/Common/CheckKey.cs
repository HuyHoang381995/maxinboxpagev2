﻿using DeviceId;
using License.RNCryptor;
using maxcare;
using MCommon;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Common
{
    public class CheckKey
    {
        public static bool Check(string userName, string pass, int softIndex)
        {
            bool isSuccess = false;
            try
            {
                string deviceId = CommonCSharp.Md5Encode(new DeviceIdBuilder().AddMachineName().AddProcessorId().AddMotherboardSerialNumber().AddSystemDriveSerialNumber().ToString());
                if (userName == "" || pass == "" || CommonCSharp.IsValidMail(userName) == false)
                {
                    goto Xong;
                }

                RequestXNet request = new RequestXNet("","","",0);
                string api_token = CommonCSharp.ReadHTMLCode("http://app.minsoftware.vn/api/auth?datavery=" + CommonCSharp.Base64Encode(userName + "|" + pass)).Replace("\"", "");
                if (api_token.Trim() != "")
                {
                    goto Xong;
                }
                Random rd = new Random();
                int codekeyrandom = rd.Next(100000, 999999);
                string url = "http://app.minsoftware.vn/minapi/minapi/api.php/Check?data=";
                string strRequest = deviceId + "|" + api_token + "|" + softIndex + "|" + codekeyrandom + "|" + "minsoftware0803";

                Encryptor encrypt = new Encryptor();
                string strEncryptRequest = encrypt.Encrypt(strRequest, "thangtungmin080394");

                string checkLisence = request.RequestGet(url + strEncryptRequest).Replace("\"", "");
                checkLisence = CommonCSharp.Base64Decode(checkLisence);

                Decryptor decrypt = new Decryptor();
                checkLisence = decrypt.Decrypt(checkLisence, "thangtungmin080394");
                if (checkLisence.Contains("chuakichhoat") || checkLisence.Contains("error") || checkLisence.Contains("hethan"))
                {
                    goto Xong;
                }

                string full_name = checkLisence.Split('|')[0];
                string api_token_sv = checkLisence.Split('|')[1];
                string date_exp = checkLisence.Split('|')[2];
                string device_server = checkLisence.Split('|')[3];
                string codekeyrandom_sv = checkLisence.Split('|')[4];
                string keystatic_sv = checkLisence.Split('|')[5];

                if (deviceId != device_server || api_token_sv != api_token || codekeyrandom_sv != codekeyrandom.ToString() || keystatic_sv != "minsoftware0803")
                {
                    goto Xong;
                }
                else
                {
                    isSuccess = true;
                }
            }
            catch
            {
            }
            Xong:
            return isSuccess;
        }
        public static void CheckVersion(string softname = "test")
        {
            string hostname = "https://minsoftware.xyz/file/" + softname + "/";
            try
            {
                WebClient ud = new WebClient();
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                //ud.DownloadFileCompleted += new AsyncCompletedEventHandler(udcom);
                Uri update = new Uri(hostname + "update.ini");
                ud.DownloadFile(update, Base.currentPath + "update/update.ini");

                //read server update file
                CommonIniFile ini_server = new CommonIniFile(Base.currentPath + "update/update.ini");
                string new_version = ini_server.Read("Version", softname);
                double dNewVersion = Convert.ToDouble(new_version.Replace(".", "").Insert(1, "."));
                //read local update file
                CommonIniFile ini_local = new CommonIniFile(Base.currentPath + "update.ini");
                string old_version = ini_local.Read("Version", softname);
                double dOldVersion = Convert.ToDouble(old_version.Replace(".", "").Insert(1, "."));

                if (dNewVersion > dOldVersion)
                {
                    string content = "\r\n"+"Version: " + new_version;
                    content += "\r\n" + "Nội dung update:";
                    content += "\r\n" + CommonCSharp.Base64Decode(ini_server.Read("Content", softname));
                    content += "\r\n\r\n" + "Bạn có muốn cập nhật phần mềm?";
                    if (MessageBox.Show("Đã có bản cập nhật mới!" + "\r\n" + content, "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        Process.Start(Base.currentPath + "AutoUpdate.exe");
                        MCommon.Common.KillProcess(softname);
                    }
                }
                else
                {
                }
            }
            catch
            {
            }
        }
    }
}
