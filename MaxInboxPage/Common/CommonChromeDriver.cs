﻿using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Common
{
    public class CommonChromeDriver
    {
        public static ChromeDriver OpenChrome(ChromeDriver chrome, bool isHideChrome, bool isHideImage, bool isDisableSound, string UserAgent, string LinkProfile,
            Point Size, Point Position, string Proxy, int TimeWaitForSearchingElement = 0, int TimeWaitForLoadingPage = 60)
        {
            ChromeDriverService service = ChromeDriverService.CreateDefaultService();
            service.HideCommandPromptWindow = true;

            ChromeOptions options = new ChromeOptions();
            options.AddArguments(new string[] {
                "--disable-notifications",
                "--window-size="+Size.X+","+Size.Y,
                "--window-position="+Position.X+","+Position.Y,
                "--no-sandbox",
                "--disable-gpu",// applicable to windows os only
                "--disable-dev-shm-usage",//overcome limited resource problems       
                "--disable-web-security",
                "--disable-rtc-smoothness-algorithm",
                "--disable-webrtc-hw-decoding",
                "--disable-webrtc-hw-encoding",
                "--disable-webrtc-multiple-routes",
                "--disable-webrtc-hw-vp8-encoding",
                "--enforce-webrtc-ip-permission-check",
                "--force-webrtc-ip-handling-policy",
                "--ignore-certificate-errors",
                "--disable-infobars",
                "--disable-popup-blocking"
            });
            options.AddUserProfilePreference("profile.default_content_setting_values.notifications", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.plugins", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.popups", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.geolocation", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.auto_select_certificate", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.mixed_script", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.media_stream", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.media_stream_mic", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.media_stream_camera", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.protocol_handlers", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.midi_sysex", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.push_messaging", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.ssl_cert_decisions", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.metro_switch_to_desktop", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.protected_media_identifier", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.site_engagement", 1);
            options.AddUserProfilePreference("profile.default_content_setting_values.durable_storage", 1);
            //options.AddUserProfilePreference("profile.managed_default_content_settings.images", 2);//hide image
            options.AddUserProfilePreference("useAutomationExtension", true);

            if (isDisableSound)
            {
                options.AddArgument("--mute-audio");
            }

            if (!isHideChrome)
            {
                if (isHideImage)
                    options.AddArgument("--blink-settings=imagesEnabled=false");

                if (!string.IsNullOrEmpty(LinkProfile.Trim()))
                    options.AddArgument("--user-data-dir=" + LinkProfile);
            }
            else
            {
                options.AddArgument("--blink-settings=imagesEnabled=false");
                options.AddArgument("--headless");
            }
            if (!string.IsNullOrEmpty(Proxy.Trim()))
                options.AddArgument("--proxy-server= 127.0.0.1:" + Proxy);

            if (!string.IsNullOrEmpty(UserAgent.Trim()))
                options.AddArgument("--user-agent=" + UserAgent);

            chrome = new ChromeDriver(service, options);

            chrome.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(TimeWaitForSearchingElement);
            chrome.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(TimeWaitForLoadingPage);
            return chrome;
        }
        public static void QuitChrome(ChromeDriver chrome)
        {
            try
            {
                chrome.Quit();
            }
            catch
            { }
        }

        public static bool CheckChromeClosed(ChromeDriver chrome)
        {
            bool isClosed = true;
            try
            {
                var x = chrome.Title;
                isClosed = false;
            }
            catch
            { }
            return isClosed;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeAttribute">1-Id, 2-Name, 3-Xpath</param>
        /// <param name="attributeValue"></param>
        /// <returns></returns>
        public static bool CheckExistElement(ChromeDriver chrome, int typeAttribute, string attributeValue, int timeOut = 0)
        {
            bool isExist = false;
            TimeSpan timeImplicitWait = chrome.Manage().Timeouts().ImplicitWait;
            chrome.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(0);

            int timeStart = Environment.TickCount;
            while (true)
            {
                switch (typeAttribute)
                {
                    case 1:
                        isExist = chrome.FindElementsById(attributeValue).Count > 0;
                        break;
                    case 2:
                        isExist = chrome.FindElementsByName(attributeValue).Count > 0;
                        break;
                    case 3:
                        isExist = chrome.FindElementsByXPath(attributeValue).Count > 0;
                        break;
                    default:
                        break;
                }

                if (isExist)
                {
                    break;
                }
                else
                {
                    if (Environment.TickCount - timeStart > timeOut * 1000)
                    {
                        break;
                    }
                }
            }

            chrome.Manage().Timeouts().ImplicitWait = timeImplicitWait;
            return isExist;
        }

        public static bool NavigateChrome(ChromeDriver chrome, string url)
        {
            bool isSuccess = false;
            try
            {
                chrome.Navigate().GoToUrl(url);
                isSuccess = true;
            }
            catch
            { }
            return isSuccess;
        }
        
        public static bool ScrollChrome(ChromeDriver chrome, int x, int y)
        {
            bool isSuccess = false;
            try
            {
                var js1 = String.Format("window.scrollTo({0}, {1})", x, y);
                chrome.ExecuteScript(js1);
                isSuccess = true;
            }
            catch
            {
            }
            return isSuccess;
        }

        public static bool SendKeysChrome(ChromeDriver chrome, int typeAttribute, string attributeValue, string content, double timeDelay)
        {
            bool isSuccess = false;
            try
            {
                for (int i = 0; i < content.Length; i++)
                {
                    switch (typeAttribute)
                    {
                        case 1:
                            chrome.FindElementById(attributeValue).SendKeys(content[i].ToString());
                            break;
                        case 2:
                            chrome.FindElementByName(attributeValue).SendKeys(content[i].ToString());
                            break;
                        case 3:
                            chrome.FindElementByXPath(attributeValue).SendKeys(content[i].ToString());
                            break;
                        default:
                            break;
                    }

                    if (i < content.Length - 1)
                    {
                        Thread.Sleep(Convert.ToInt32(timeDelay * 1000));
                    }
                }
                isSuccess = true;
            }
            catch
            {}
            return isSuccess;
        }
        public static bool SendKeysChrome(ChromeDriver chrome, int typeAttribute, string attributeValue, string content)
        {
            bool isSuccess = false;
            try
            {
                switch (typeAttribute)
                {
                    case 1:
                        chrome.FindElementById(attributeValue).SendKeys(content);
                        break;
                    case 2:
                        chrome.FindElementByName(attributeValue).SendKeys(content);
                        break;
                    case 3:
                        chrome.FindElementByXPath(attributeValue).SendKeys(content);
                        break;
                    default:
                        break;
                }
                isSuccess = true;
            }
            catch
            {
            }
            return isSuccess;
        }
        public static bool ClickChrome(ChromeDriver chrome, int typeAttribute, string attributeValue)
        {
            bool isSuccess = false;
            try
            {
                switch (typeAttribute)
                {
                    case 1:
                        chrome.FindElementById(attributeValue).Click();
                        break;
                    case 2:
                        chrome.FindElementByName(attributeValue).Click();
                        break;
                    case 3:
                        chrome.FindElementByXPath(attributeValue).Click();
                        break;
                    default:
                        break;
                }
                isSuccess = true;
            }
            catch
            {
            }
            return isSuccess;
        }

        public static bool ExecuteScriptChrome(ChromeDriver chrome,string script)
        {
            bool isSuccess = false;
            try
            {
                chrome.ExecuteScript(script);
                isSuccess = true;
            }
            catch
            {
            }
            return isSuccess;
        }
    }
}
