﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MCommon
{
    class TinsoftProxy
    {
        public string api_key { get; set; }

        public string proxy { get; set; }
        public string ip { get; set; }
        public int port { get; set; }
        public int timeout { get; set; }
        public int next_change { get; set; }
        public int location { get; set; }

        public TinsoftProxy(string api_key, int limit_theads_use, int location = 0)
        {
            this.api_key = api_key;
            this.proxy = "";
            this.ip = "";
            this.port = 0;
            this.timeout = 0;
            this.next_change = 0;
            this.location = location;

            this.limit_theads_use = limit_theads_use;
            dangSuDUng = 0;
            daSuDung = 0;
            canChangeIP = true;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns>0 không đổi được proxy, 1 đổi thành công, -1 đạt số lượng tối đa + co the doi proxy, -2 đạt số lượng tối đa + dang dc su dung</returns>
        /// 

        object k1 = new object();
        public string tryToGetMyIP()
        {
            //Th1: canChangeIP=true => Change Proxy(){1}; Gán canChangeIP=false, [đã sử dụng]++;
            //Th2: canChangeIP=false
            //Th2.1: [đã sử dụng] < [max sử dụng] => Lấy luôn proxy hiện tại{2},  [đã sử dụng]++;
            //Th2.2: [đã sử dụng] = [max sử dụng] => {3};

            lock (k1)
            {
                if (canChangeIP)
                {
                    if (dangSuDUng > 1)
                    {
                        daSuDung--;
                        dangSuDUng--;
                        return "-2";
                    }
                    else
                    {
                        if (changeProxy())
                        {
                            if (daSuDung > limit_theads_use)
                            {
                                daSuDung = 0;
                                canChangeIP = true;
                            }
                            else
                                canChangeIP = false;
                            return "1";
                        }
                        else
                        {
                            //dangSuDUng--;
                            //daSuDung--;
                            return "0";
                        }
                    }
                }
                else
                {
                    if (daSuDung <= limit_theads_use)
                    {
                        return "1";
                    }
                    else
                    {
                        daSuDung--;
                        canChangeIP = true;
                        return "-1";
                    }
                }
            }
        }

        object k = new object();
        public void DecrementDangSuDung()
        {
            lock (k)
            {
                dangSuDUng--;
                if (dangSuDUng == 0)
                {
                    daSuDung = 0;
                    canChangeIP = true;
                }
            }
        }


        public bool changeProxy()
        {
            if (this.checkLastRequest())
            {
                isChangingIp = true;
                this.errorCode = "";
                this.next_change = 0;
                this.proxy = "";
                this.ip = "";
                this.port = 0;
                this.timeout = 0;
                string svcontent = this.getSVContent(string.Concat(new object[]
                {
                    this.svUrl,
                    "/api/changeProxy.php?key=",
                    this.api_key,
                    "&location=",
                    this.location
                }));
                if (svcontent != "")
                {
                    try
                    {
                        JObject jobject = JObject.Parse(svcontent);
                        if (bool.Parse(jobject["success"].ToString()))
                        {
                            this.proxy = jobject["proxy"].ToString();
                            string[] array = this.proxy.Split(new char[]
                            {
                                ':'
                            });
                            this.ip = array[0];
                            this.port = int.Parse(array[1]);
                            this.timeout = int.Parse(jobject["timeout"].ToString());
                            this.next_change = int.Parse(jobject["next_change"].ToString());
                            this.errorCode = "";
                            DateTime dateTime = new DateTime(2001, 1, 1);
                            long ticks = DateTime.Now.Ticks - dateTime.Ticks;
                            TimeSpan timeSpan = new TimeSpan(ticks);
                            this.time_ip_changed = (int)timeSpan.TotalSeconds;
                            isChangingIp = false;
                            return true;
                        }
                        this.errorCode = jobject["description"].ToString();
                        this.next_change = int.Parse(jobject["next_change"].ToString());
                    }
                    catch
                    {
                    }
                }
                else
                {
                    this.errorCode = "request server timeout!";
                }
            }
            else
            {
                this.errorCode = "Request so fast!";
            }
            isChangingIp = false;
            return false;
        }

        private bool checkLastRequest()
        {
            try
            {
                DateTime dateTime = new DateTime(2001, 1, 1);
                long ticks = DateTime.Now.Ticks - dateTime.Ticks;
                TimeSpan timeSpan = new TimeSpan(ticks);
                int num = (int)timeSpan.TotalSeconds;
                if (num - this.lastRequest >= 10)
                {
                    this.lastRequest = num;
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }

        private string getSVContent(string url)
        {
            Console.WriteLine(url);
            string text = "";
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    text = webClient.DownloadString(url);
                }
                if (string.IsNullOrEmpty(text))
                {
                    text = "";
                }
            }
            catch
            {
                text = "";
            }
            return text;
        }

        public string errorCode = "";

        private string svUrl = "http://proxy.tinsoftsv.com";

        private int lastRequest = 0;


        public int time_ip_changed = 0;



        private bool isStop = false;

        //đang đổi ip
        public bool isChangingIp = false;

        //có thể đổi
        public bool canChangeIP = true;



        //Đã được chọn, chờ dự bị
        public bool daChon = false;

        //số lượng đang sử dụng
        public int dangSuDUng = 0;

        //đã sử dụng
        public int daSuDung = 0;

        //số lượng tối đa cùng lúc
        public int limit_theads_use = 3;
    }
}
