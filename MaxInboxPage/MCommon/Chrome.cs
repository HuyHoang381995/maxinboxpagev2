﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Keys = OpenQA.Selenium.Keys;

namespace MCommon
{
    public class Chrome
    {
        private ChromeDriver chrome;
        /// <summary>
        /// Hide Browser(default: false)
        /// </summary>
        public bool HideBrowser { get; set; }
        public bool Incognito { get; set; }
        /// <summary>
        /// Disable Image(default: false)
        /// </summary>
        public bool DisableImage { get; set; }
        /// <summary>
        /// Disable Sound(default: false)
        /// </summary>
        public bool DisableSound { get; set; }
        public bool AutoPlayVideo { get; set; }
        /// <summary>
        /// UserAgent(default: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36)
        /// </summary>
        public string UserAgent { get; set; }
        public string ProfilePath { get; set; }
        /// <summary>
        /// Browser Size(default: (300,300))
        /// </summary>
        public Point Size { get; set; }
        /// <summary>
        /// Browser Heigh(default: 300)
        /// </summary>
        public int Size_Heigh { get; set; }
        /// <summary>
        /// Browser Width(default: 300)
        /// </summary>
        public int Size_Width { get; set; }
        /// <summary>
        /// Browser Position(default: (0,0))
        /// </summary>
        public Point Position { get; set; }
        /// <summary>
        /// Browser PositionX(default: 0)
        /// </summary>
        public int Position_X { get; set; }
        /// <summary>
        /// Browser PositionY(default: 0)
        /// </summary>
        public int Position_Y { get; set; }
        /// <summary>
        /// Time Wait For Searching Element (seconds - default: 0)
        /// </summary>
        public int TimeWaitForSearchingElement { get; set; }
        /// <summary>
        /// Time Wait For Loading Page (minutes - default: 5)
        /// </summary>
        public int TimeWaitForLoadingPage { get; set; }
        /// <summary>
        /// Socks5 proxy or Port 911
        /// </summary>
        public string Proxy { get; set; }
        public int TypeProxy { get; set; }
        public string App { get; set; }

        public Chrome()
        {
            HideBrowser = false;
            DisableImage = false;
            DisableSound = false;
            Incognito = false;
            UserAgent = "";
            ProfilePath = "";
            Size_Heigh = 300;
            Size_Width = 300;
            Size = new Point(Size_Width, Size_Heigh);
            Position_X = 0;
            Position_Y = 0;
            Proxy = "";
            TypeProxy = 0;
            Position = new Point(Position_X, Position_Y);
            TimeWaitForSearchingElement = 0;
            TimeWaitForLoadingPage = 5;
            App = "";
            AutoPlayVideo = false;
        }

        //public Chrome(bool hideBrowser = false, bool disableImage = false, bool disableSound = false, string userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36", 
        //    string profilePath = "", Point size = default(Point), Point position = default(Point), string proxy = "", int timeWaitForSearchingElement = 0, int timeWaitForLoadingPage = 5,string app = "https://facebook.com")
        //{
        //    this.HideBrowser = hideBrowser;
        //    this.DisableImage = disableImage;
        //    this.DisableSound = disableSound;
        //    this.UserAgent = userAgent;
        //    this.ProfilePath = profilePath;
        //    this.Size = size;
        //    this.Position = position;
        //    this.Proxy = proxy;
        //    this.TimeWaitForSearchingElement = timeWaitForSearchingElement;
        //    this.TimeWaitForLoadingPage = timeWaitForLoadingPage;
        //    this.App = app;
        //}
        public bool Open()
        {
            bool isSuccess = false;
            try
            {
                ChromeDriverService service = ChromeDriverService.CreateDefaultService();
                service.HideCommandPromptWindow = true;

                ChromeOptions options = new ChromeOptions();
                options.AddArguments(new string[] {
                    "--disable-notifications",
                    "--window-size="+this.Size.X+","+this.Size.Y,
                    "--window-position="+Position.X+","+Position.Y,
                    "--no-sandbox",
                    "--disable-gpu",// applicable to windows os only
                    "--disable-dev-shm-usage",//overcome limited resource problems       
                    "--disable-web-security",
                    "--disable-rtc-smoothness-algorithm",
                    "--disable-webrtc-hw-decoding",
                    "--disable-webrtc-hw-encoding",
                    "--disable-webrtc-multiple-routes",
                    "--disable-webrtc-hw-vp8-encoding",
                    "--enforce-webrtc-ip-permission-check",
                    "--force-webrtc-ip-handling-policy",
                    "--ignore-certificate-errors",
                    "--disable-infobars",
                    "--disable-blink-features=\"BlockCredentialedSubresources\"",
                    "--disable-popup-blocking"
                });
                options.AddUserProfilePreference("profile.default_content_setting_values.notifications", 1);
                options.AddUserProfilePreference("profile.default_content_setting_values.plugins", 1);
                options.AddUserProfilePreference("profile.default_content_setting_values.popups", 1);
                options.AddUserProfilePreference("profile.default_content_setting_values.geolocation", 0);
                options.AddUserProfilePreference("profile.default_content_setting_values.auto_select_certificate", 1);
                options.AddUserProfilePreference("profile.default_content_setting_values.mixed_script", 1);
                options.AddUserProfilePreference("profile.default_content_setting_values.media_stream", 1);
                options.AddUserProfilePreference("profile.default_content_setting_values.media_stream_mic", 1);
                options.AddUserProfilePreference("profile.default_content_setting_values.media_stream_camera", 1);
                options.AddUserProfilePreference("profile.default_content_setting_values.protocol_handlers", 1);
                options.AddUserProfilePreference("profile.default_content_setting_values.midi_sysex", 1);
                options.AddUserProfilePreference("profile.default_content_setting_values.push_messaging", 1);
                options.AddUserProfilePreference("profile.default_content_setting_values.ssl_cert_decisions", 1);
                options.AddUserProfilePreference("profile.default_content_setting_values.metro_switch_to_desktop", 1);
                options.AddUserProfilePreference("profile.default_content_setting_values.protected_media_identifier", 1);
                options.AddUserProfilePreference("profile.default_content_setting_values.site_engagement", 1);
                options.AddUserProfilePreference("profile.default_content_setting_values.durable_storage", 1);
                options.AddUserProfilePreference("useAutomationExtension", true);

                if (DisableSound)
                {
                    options.AddArgument("--mute-audio");
                }


                if (!HideBrowser)
                {
                    if (DisableImage)
                        options.AddArgument("--blink-settings=imagesEnabled=false");

                    if (!string.IsNullOrEmpty(ProfilePath.Trim()))
                        options.AddArgument("--user-data-dir=" + ProfilePath);
                }
                else
                {
                    options.AddArgument("--blink-settings=imagesEnabled=false");
                    options.AddArgument("--headless");
                }

                if (Incognito)
                {
                    options.AddArguments("--incognito");
                }

                if (!string.IsNullOrEmpty(Proxy.Trim()))
                {
                    int dem = Proxy.Split(':').Count();
                    switch (dem)
                    {
                        case 1:
                            if (TypeProxy == 0)
                                options.AddArgument("--proxy-server= 127.0.0.1:" + Proxy);
                            else
                                options.AddArgument("--proxy-server= socks5://127.0.0.1:" + Proxy);
                            break;
                        case 2:
                            if (TypeProxy == 0)
                                options.AddArgument("--proxy-server= " + Proxy);
                            else
                                options.AddArgument("--proxy-server= socks5://" + Proxy);
                            break;
                        case 4:
                            if (TypeProxy == 0)
                            {
                                //options.AddExtension(@"extension\Proxy Auto Auth.crx");
                                //options.AddArgument(string.Format("--proxy-server={0}:{1}", Proxy.Split(':')[0], Proxy.Split(':')[1]));
                                options.AddArgument("--proxy-server= " + Proxy.Split(':')[0] + ":" + Proxy.Split(':')[1]);
                                options.AddExtension("extension\\proxy.crx");
                            }
                            else
                            {
                                //options.AddExtension(@"extension\Proxy Auto Auth.crx");
                                //options.AddArgument(string.Format("--proxy-server=socks5://{0}:{1}", Proxy.Split(':')[0], Proxy.Split(':')[1]));
                                options.AddArgument("--proxy-server= socks5://" + Proxy.Split(':')[0] + ":" + Proxy.Split(':')[1]);
                                options.AddExtension("extension\\proxy.crx");
                            }
                            break;
                        default:
                            break;
                    }
                }

                if (!string.IsNullOrEmpty(App.Trim()))
                {
                    options.AddArgument("--app= " + App);
                }

                //Tùng
                if (UserAgent != "")
                {
                    if (Proxy.Split(':').Count() == 4)
                        options.AddArgument($"--user-agent={UserAgent}$PC${Proxy.Split(':')[2] + ":" + Proxy.Split(':')[3]}");
                    else
                        options.AddArgument($"--user-agent={UserAgent}");
                }

                if (AutoPlayVideo)
                {
                    options.AddArgument("--autoplay-policy=no-user-gesture-required");
                }

                chrome = new ChromeDriver(service, options);

                chrome.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(TimeWaitForSearchingElement);
                chrome.Manage().Timeouts().PageLoad = TimeSpan.FromMinutes(TimeWaitForLoadingPage);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                ExportError(null, ex, "chrome.Open()");
            }
            return isSuccess;
        }


        public void ScrollSmooth(int distance)
        {
            try
            {
                chrome.ExecuteScript("window.scrollBy({ top: " + distance + ",behavior: 'smooth'});");
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.ScrollSmooth({distance})");
            }
        }


        public string GetUseragent()
        {
            string ua = "";
            try
            {
                ua = chrome.ExecuteScript("return navigator.userAgent").ToString();
            }
            catch
            {
            }
            return ua;
        }
        public bool SendKeyDown(int typeAttribute, string attributeValue)
        {
            bool isSuccess = false;
            try
            {
                switch (typeAttribute)
                {
                    case 1:
                        chrome.FindElementById(attributeValue).SendKeys(Keys.ArrowDown);
                        break;
                    case 2:
                        chrome.FindElementByName(attributeValue).SendKeys(Keys.ArrowDown);
                        break;
                    case 3:
                        chrome.FindElementByXPath(attributeValue).SendKeys(Keys.ArrowDown);
                        break;
                    case 4:
                        chrome.FindElementByCssSelector(attributeValue).SendKeys(Keys.ArrowDown);
                        break;
                    default:
                        break;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.SendKeyDown({typeAttribute},{attributeValue})");
            }
            return isSuccess;
        }
        public string GetURL()
        {
            try
            {
                return chrome.Url;
            }
            catch (Exception ex)
            {
                ExportError(null, ex, "chrome.GetURL()");
            }
            return "";
        }
        public bool GotoURL(string url)
        {
            bool isSuccess = false;
            try
            {
                chrome.Navigate().GoToUrl(url);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.GotoURL({url})");
            }
            return isSuccess;
        }
        public bool Refresh()
        {
            bool isSuccess = false;
            try
            {
                chrome.Navigate().Refresh();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.Refresh()");
            }
            return isSuccess;
        }
        public bool GotoBackPage()
        {
            bool isSuccess = false;
            try
            {
                chrome.Navigate().Back();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.GotoBackPage()");
            }
            return isSuccess;
        }
        public bool HoverElement(int typeAttribute, string attributeValue, double timeHover_second)
        {
            try
            {
                switch (typeAttribute)
                {
                    case 1:
                        new Actions(chrome).MoveToElement(chrome.FindElement(By.Id(attributeValue))).Perform();
                        break;
                    case 2:
                        new Actions(chrome).MoveToElement(chrome.FindElement(By.Name(attributeValue))).Perform();
                        break;
                    case 3:
                        new Actions(chrome).MoveToElement(chrome.FindElement(By.XPath(attributeValue))).Perform();
                        break;
                    case 4:
                        new Actions(chrome).MoveToElement(chrome.FindElement(By.CssSelector(attributeValue))).Perform();
                        break;
                    default:
                        break;
                }
                Thread.Sleep(Convert.ToInt32(timeHover_second * 1000));

                return true;
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.HoverElement({typeAttribute}, {attributeValue}, {timeHover_second})");
            }
            return false;
        }
        public object ExecuteScript(string script)
        {
            try
            {
                return chrome.ExecuteScript(script);
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.ExecuteScript({script})");
            }
            return "";
        }
        public bool Click(int typeAttribute, string attributeValue)
        {
            bool isSuccess = false;
            try
            {
                switch (typeAttribute)
                {
                    case 1:
                        chrome.FindElementById(attributeValue).Click();
                        break;
                    case 2:
                        chrome.FindElementByName(attributeValue).Click();
                        break;
                    case 3:
                        chrome.FindElementByXPath(attributeValue).Click();
                        break;
                    case 4:
                        chrome.FindElementByCssSelector(attributeValue).Click();
                        break;
                    default:
                        break;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.Click({typeAttribute},{attributeValue})");
            }
            return isSuccess;
        }
        public bool Click(string selector, int index, string Subselector)
        {
            bool isSuccess = false;
            try
            {
                if(Subselector!="")
                chrome.FindElementsByCssSelector(selector)[index].FindElement(By.CssSelector(Subselector)).Click();
                else
                    chrome.FindElementsByCssSelector(selector)[index].Click();
                isSuccess = true;
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.Click({selector},{index},{selector})");
            }
            return isSuccess;
        }
        public bool ClickWithAction(int typeAttribute, string attributeValue)
        {
            bool isSuccess = false;
            try
            {
                switch (typeAttribute)
                {
                    case 1:
                        new Actions(chrome).Click(chrome.FindElementById(attributeValue)).Perform();
                        break;
                    case 2:
                        new Actions(chrome).Click(chrome.FindElementByName(attributeValue)).Perform();
                        break;
                    case 3:
                        new Actions(chrome).Click(chrome.FindElementByXPath(attributeValue)).Perform();
                        break;
                    case 4:
                        new Actions(chrome).Click(chrome.FindElementByCssSelector(attributeValue)).Perform();
                        break;
                    default:
                        break;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.ClickWithAction({typeAttribute},{attributeValue})");
            }
            return isSuccess;
        }
        public bool SendKeys(int typeAttribute, string attributeValue, string content, bool isClick = true)
        {
            bool isSuccess = false;
            try
            {
                if (isClick)
                    Click(typeAttribute, attributeValue);
                switch (typeAttribute)
                {
                    case 1:
                        chrome.FindElementById(attributeValue).SendKeys(content);
                        break;
                    case 2:
                        chrome.FindElementByName(attributeValue).SendKeys(content);
                        break;
                    case 3:
                        chrome.FindElementByXPath(attributeValue).SendKeys(content);
                        break;
                    case 4:
                        chrome.FindElementByCssSelector(attributeValue).SendKeys(content);
                        break;
                    default:
                        break;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.SendKeys({typeAttribute},{attributeValue},{content},{isClick})");
            }
            return isSuccess;
        }
        public bool SendKeys(int typeAttribute, string attributeValue, string content, double timeDelay_Second, bool isClick = true)
        {
            bool isSuccess = false;
            try
            {
                if (isClick)
                    Click(typeAttribute, attributeValue);
                for (int i = 0; i < content.Length; i++)
                {
                    switch (typeAttribute)
                    {
                        case 1:
                            chrome.FindElementById(attributeValue).SendKeys(content[i].ToString());
                            break;
                        case 2:
                            chrome.FindElementByName(attributeValue).SendKeys(content[i].ToString());
                            break;
                        case 3:
                            chrome.FindElementByXPath(attributeValue).SendKeys(content[i].ToString());
                            break;
                        case 4:
                            chrome.FindElementByCssSelector(attributeValue).SendKeys(content[i].ToString());
                            break;
                        default:
                            break;
                    }

                    Thread.Sleep(Convert.ToInt32(timeDelay_Second * 1000));
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.SendKeys({typeAttribute},{attributeValue},{content},{timeDelay_Second},{isClick})");
            }
            return isSuccess;
        }
        public bool SelectText(int typeAttribute, string attributeValue)
        {
            bool isSuccess = false;
            try
            {
                switch (typeAttribute)
                {
                    case 1:
                        chrome.FindElementById(attributeValue).SendKeys(Keys.Control + "a");
                        break;
                    case 2:
                        chrome.FindElementByName(attributeValue).SendKeys(Keys.Control + "a");
                        break;
                    case 3:
                        chrome.FindElementByXPath(attributeValue).SendKeys(Keys.Control + "a");
                        break;
                    case 4:
                        chrome.FindElementByCssSelector(attributeValue).SendKeys(Keys.Control + "a");
                        break;
                    default:
                        break;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.SelectText({typeAttribute},{attributeValue})");
            }
            return isSuccess;
        }
        public bool ClearText(int typeAttribute, string attributeValue)
        {
            bool isSuccess = false;
            try
            {
                switch (typeAttribute)
                {
                    case 1:
                        chrome.FindElementById(attributeValue).Clear();
                        break;
                    case 2:
                        chrome.FindElementByName(attributeValue).Clear();
                        break;
                    case 3:
                        chrome.FindElementByXPath(attributeValue).Clear();
                        break;
                    case 4:
                        chrome.FindElementByCssSelector(attributeValue).Clear();
                        break;
                    default:
                        break;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.ClearText({typeAttribute},{attributeValue})");
            }
            return isSuccess;
        }
        public bool CheckExistElement(string querySelector, double timeWait_Second = 0)
        {
            bool isExist = true;
            try
            {
                int timeStart = Environment.TickCount;
                while ((string)chrome.ExecuteScript("return document.querySelectorAll('" + querySelector + "').length+''") == "0")
                {
                    if (Environment.TickCount - timeStart > timeWait_Second * 1000)
                    {
                        isExist = false;
                        break;
                    }
                    Thread.Sleep(1000);
                }
            }
            catch (Exception ex)
            {
                isExist = false;
                ExportError(null, ex, $"chrome.CheckExistElement({querySelector},{timeWait_Second})");
            }
            return isExist;
        }
        public bool CheckExistElementv2(string JSPath, double timeWait_Second = 0)
        {
            bool isExist = true;
            try
            {
                int timeStart = Environment.TickCount;
                while ((string)chrome.ExecuteScript("return " + JSPath + ".length+''") == "0")
                {
                    if (Environment.TickCount - timeStart > timeWait_Second * 1000)
                    {
                        isExist = false;
                        break;
                    }
                    Thread.Sleep(1000);
                }
            }
            catch (Exception ex)
            {
                isExist = false;
                ExportError(null, ex, $"chrome.CheckExistElement({JSPath},{timeWait_Second})");
            }
            return isExist;
        }
        public bool CheckChromeClosed()
        {
            bool isClosed = true;
            try
            {
                var x = chrome.Title;
                isClosed = false;
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.CheckChromeClosed()");
            }
            return isClosed;
        }

        /// <summary>
        /// Wait For Element Appear or Disappear
        /// </summary>
        /// <param name="chrome"></param>
        /// <param name="querySelector"></param>
        /// <param name="timeWait_Second"></param>
        /// <param name="typeSearch">0-Wait For Element Appear, 1-Wait For Element Disappear</param>
        /// <returns></returns>
        public bool WaitForSearchElement(string querySelector, int typeSearch = 0, double timeWait_Second = 0)
        {
            bool isDone = true;
            try
            {
                int timeStart = Environment.TickCount;
                if (typeSearch == 0)
                {
                    while ((string)chrome.ExecuteScript("return document.querySelectorAll('" + querySelector + "').length+''") == "0")
                    {
                        if (Environment.TickCount - timeStart > timeWait_Second * 1000)
                        {
                            isDone = false;
                            break;
                        }
                        Thread.Sleep(1000);
                    }
                }
                else
                {
                    while ((string)chrome.ExecuteScript("return document.querySelectorAll('" + querySelector + "').length+''") != "0")
                    {
                        if (Environment.TickCount - timeStart > timeWait_Second * 1000)
                        {
                            isDone = false;
                            break;
                        }
                        Thread.Sleep(1000);
                    }
                }
            }
            catch (Exception ex)
            {
                isDone = false;
                ExportError(null, ex, $"chrome.WaitForSearchElement({querySelector},{typeSearch},{timeWait_Second})");
            }
            return isDone;
        }
        public int CheckExistElements(double timeWait_Second = 0, params string[] querySelectors)
        {
            int result = 0;
            try
            {
                int timeStart = Environment.TickCount;
                while (true)
                {
                    for (int i = 0; i < querySelectors.Length; i++)
                    {
                        if (CheckExistElement(querySelectors[i]))
                        {
                            return (i + 1);
                        }
                    }

                    if (Environment.TickCount - timeStart > timeWait_Second * 1000)
                        break;
                    Thread.Sleep(1000);
                }
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.CheckExistElements({timeWait_Second},{string.Join("|", querySelectors)})");
            }
            return result;
        }

        public bool SendEnter(int typeAttribute, string attributeValue)
        {
            bool isSuccess = false;
            try
            {
                switch (typeAttribute)
                {
                    case 1:
                        chrome.FindElementById(attributeValue).SendKeys(Keys.Enter);
                        break;
                    case 2:
                        chrome.FindElementByName(attributeValue).SendKeys(Keys.Enter);
                        break;
                    case 3:
                        chrome.FindElementByXPath(attributeValue).SendKeys(Keys.Enter);
                        break;
                    case 4:
                        chrome.FindElementByCssSelector(attributeValue).SendKeys(Keys.Enter);
                        break;
                    default:
                        break;
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.SendEnter({typeAttribute},{attributeValue})");
            }
            return isSuccess;
        }
        public bool Scroll(int x, int y)
        {
            bool isSuccess = false;
            try
            {
                var js1 = String.Format("window.scrollTo({0}, {1})", x, y);
                chrome.ExecuteScript(js1);
                isSuccess = true;
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.Scroll({x},{y})");
            }
            return isSuccess;
        }
        public void ScrollSmooth(string JSpath)
        {
            try
            {
                chrome.ExecuteScript(JSpath + ".scrollIntoView({ behavior: 'smooth', block: 'center'});");
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.ScrollSmooth({JSpath})");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="JSpath"></param>
        /// <returns>-2:error</returns>
        public int CheckExistElementOnScreen(string JSpath)
        {
            int check = -2;
            try
            {
                check = Convert.ToInt32(chrome.ExecuteScript("var check='';x=" + JSpath + ";if(x.getBoundingClientRect().top<=0) check='-1'; else if(x.getBoundingClientRect().top+x.getBoundingClientRect().height>window.innerHeight) check='1'; else check='0'; return check;"));
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.CheckExistElementOnScreen({JSpath})");
            }
            return check;
        }
        public Point GetSizeChrome()
        {
            Point point = new Point(0, 0);
            try
            {
                string temp = chrome.ExecuteScript("return window.innerHeight+'|'+window.innerWidth").ToString();
                point.X = Convert.ToInt32(temp.Split('|')[1]);
                point.Y = Convert.ToInt32(temp.Split('|')[0]);
            }
            catch
            {
            }
            return point;
        }
        public void Close()
        {
            try
            {
                chrome.Quit();
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.Close()");
            }
        }
        public bool ScreenCapture(string imagePath, string fileName)
        {
            bool isSuccess = false;
            try
            {
                Screenshot image = ((ITakesScreenshot)chrome).GetScreenshot();
                image.SaveAsFile(imagePath + (imagePath.EndsWith(@"\") ? "" : @"\") + fileName + ".png");
                isSuccess = true;
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.ScreenCapture({imagePath},{fileName})");
            }
            return isSuccess;
        }
        public void AddCookieIntoChrome(string cookie, string domain = ".facebook.com")
        {
            try
            {
                string[] arrData = cookie.Split(';');
                foreach (string item in arrData)
                {
                    if (item.Trim() != "")
                    {
                        string[] pars = item.Split('=');
                        if (pars.Count() > 1 && pars[0].Trim() != "")
                        {
                            Cookie cok = new Cookie(pars[0].Trim(), item.Substring(item.IndexOf('=') + 1).Trim(), domain, "/", DateTime.Now.AddDays(10));
                            chrome.Manage().Cookies.AddCookie(cok);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.AddCookieIntoChrome({cookie},{domain})");
            }
        }
        public string GetCookieFromChrome(string domain = "facebook")
        {
            string cookie = "";
            try
            {
                var sess = chrome.Manage().Cookies.AllCookies.ToArray();
                foreach (var item in sess)
                {
                    if (item.Domain.Contains(domain))
                        cookie += item.Name + "=" + item.Value + ";";
                }
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.GetCookieFromChrome({domain})");
            }

            return cookie;
        }

        /// <summary>
        /// After add new tab, chrome still focus in current tab. If you want focus to new tab, please use SwitchToLastTab();
        /// </summary>
        /// <param name="url"></param>
        public void OpenNewTab(string url, bool switchToLastTab = true)
        {
            try
            {
                chrome.ExecuteScript("window.open('" + url + "', '_blank').focus();");
                if (switchToLastTab)
                    chrome.SwitchTo().Window(chrome.WindowHandles.Last());
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.OpenNewTab({url},{switchToLastTab})");
            }
        }
        public void CloseCurrentTab()
        {
            try
            {
                chrome.Close();
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.CloseCurrentTab()");
            }
        }
        public void SwitchToFirstTab()
        {
            try
            {
                chrome.SwitchTo().Window(chrome.WindowHandles.First());
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.SwitchToFirstTab()");
            }
        }
        public void SwitchToLastTab()
        {
            try
            {
                chrome.SwitchTo().Window(chrome.WindowHandles.Last());
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.SwitchToLastTab()");
            }
        }
        public void DelayTime(double timeDelay_Seconds)
        {
            try
            {
                Thread.Sleep(Convert.ToInt32(timeDelay_Seconds * 1000));
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.DelayTime({timeDelay_Seconds})");
            }

        }
        /// <summary>
        /// Create 1 folder name "log" and create 2 folder name "images" and "html" into "log"
        /// </summary>
        public static void ExportError(Chrome chrome, Exception ex, string error = "")
        {
            try
            {
                if (!Directory.Exists("log"))
                    Directory.CreateDirectory("log");
                if (!Directory.Exists("log\\html"))
                    Directory.CreateDirectory("log\\html");
                if (!Directory.Exists("log\\images"))
                    Directory.CreateDirectory("log\\images");

                Random rrrd = new Random();
                string fileName = DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second + "_" + rrrd.Next(1000, 9999);

                if (chrome != null)
                {
                    string html = chrome.ExecuteScript("var markup = document.documentElement.innerHTML;return markup;").ToString();
                    chrome.ScreenCapture(@"log\images\", fileName);
                    File.WriteAllText(@"log\html\" + fileName + ".html", html);
                }

                using (StreamWriter writer = new StreamWriter(@"log\log.txt", true))
                {
                    writer.WriteLine("-----------------------------------------------------------------------------");
                    writer.WriteLine("Date: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                    writer.WriteLine("File: " + fileName);
                    if (error != "")
                        writer.WriteLine("Error: " + error);
                    writer.WriteLine();

                    if (ex != null)
                    {
                        writer.WriteLine("Type: " + ex.GetType().FullName);
                        writer.WriteLine("Message: " + ex.Message);
                        writer.WriteLine("StackTrace: " + ex.StackTrace);
                        ex = ex.InnerException;
                    }
                }
            }
            catch { }
        }
        public bool Select(int typeAttribute, string attributeValue, string value)
        {
            bool result = false;
            try
            {
                switch (typeAttribute)
                {
                    case 1:
                        new SelectElement(this.chrome.FindElementById(attributeValue)).SelectByValue(value);
                        break;
                    case 2:
                        new SelectElement(this.chrome.FindElementByName(attributeValue)).SelectByValue(value);
                        break;
                    case 3:
                        new SelectElement(this.chrome.FindElementByXPath(attributeValue)).SelectByValue(value);
                        break;
                    case 4:
                        new SelectElement(this.chrome.FindElementByCssSelector(attributeValue)).SelectByValue(value);
                        break;
                }
                result = true;
            }
            catch (Exception ex)
            {
                ExportError(null, ex, $"chrome.Select({typeAttribute},{attributeValue},{value})");
            }
            return result;
        }
    }
}
