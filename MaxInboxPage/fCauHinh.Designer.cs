﻿namespace maxcare
{
    partial class fCauHinh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.plComment = new System.Windows.Forms.Panel();
            this.txtNoiDung = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.plAnh = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAnh = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.nudSoLuongAnhFrom = new System.Windows.Forms.NumericUpDown();
            this.nudSoLuongAnhTo = new System.Windows.Forms.NumericUpDown();
            this.ckbSendAnh = new System.Windows.Forms.CheckBox();
            this.ckbNhanTinVanBan = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.ckbInboxAll = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.nudDelayTo = new System.Windows.Forms.NumericUpDown();
            this.nudCountInviteTo = new System.Windows.Forms.NumericUpDown();
            this.nudCountInviteFrom = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.nudDelayFrom = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.plComment.SuspendLayout();
            this.plAnh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSoLuongAnhFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSoLuongAnhTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDelayTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCountInviteTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCountInviteFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDelayFrom)).BeginInit();
            this.SuspendLayout();
            // 
            // plComment
            // 
            this.plComment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.plComment.Controls.Add(this.txtNoiDung);
            this.plComment.Controls.Add(this.label6);
            this.plComment.Enabled = false;
            this.plComment.Location = new System.Drawing.Point(30, 36);
            this.plComment.Name = "plComment";
            this.plComment.Size = new System.Drawing.Size(427, 194);
            this.plComment.TabIndex = 180;
            // 
            // txtNoiDung
            // 
            this.txtNoiDung.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNoiDung.Location = new System.Drawing.Point(7, 24);
            this.txtNoiDung.Name = "txtNoiDung";
            this.txtNoiDung.Size = new System.Drawing.Size(415, 165);
            this.txtNoiDung.TabIndex = 1;
            this.txtNoiDung.Text = "";
            this.txtNoiDung.WordWrap = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(134, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "Nội dung tin nhắn (0):";
            // 
            // plAnh
            // 
            this.plAnh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.plAnh.Controls.Add(this.label7);
            this.plAnh.Controls.Add(this.txtAnh);
            this.plAnh.Controls.Add(this.label9);
            this.plAnh.Controls.Add(this.label13);
            this.plAnh.Controls.Add(this.label14);
            this.plAnh.Controls.Add(this.nudSoLuongAnhFrom);
            this.plAnh.Controls.Add(this.nudSoLuongAnhTo);
            this.plAnh.Enabled = false;
            this.plAnh.Location = new System.Drawing.Point(30, 259);
            this.plAnh.Name = "plAnh";
            this.plAnh.Size = new System.Drawing.Size(427, 58);
            this.plAnh.TabIndex = 182;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(138, 16);
            this.label7.TabIndex = 159;
            this.label7.Text = "Đường dẫn folder ảnh:";
            // 
            // txtAnh
            // 
            this.txtAnh.Location = new System.Drawing.Point(143, 3);
            this.txtAnh.Name = "txtAnh";
            this.txtAnh.Size = new System.Drawing.Size(279, 23);
            this.txtAnh.TabIndex = 158;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 16);
            this.label9.TabIndex = 34;
            this.label9.Text = "Số ảnh/tin nhắn:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(270, 31);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 16);
            this.label13.TabIndex = 36;
            this.label13.Text = "ảnh";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(189, 31);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 16);
            this.label14.TabIndex = 38;
            this.label14.Text = "đến";
            // 
            // nudSoLuongAnhFrom
            // 
            this.nudSoLuongAnhFrom.Location = new System.Drawing.Point(143, 29);
            this.nudSoLuongAnhFrom.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.nudSoLuongAnhFrom.Name = "nudSoLuongAnhFrom";
            this.nudSoLuongAnhFrom.Size = new System.Drawing.Size(43, 23);
            this.nudSoLuongAnhFrom.TabIndex = 5;
            // 
            // nudSoLuongAnhTo
            // 
            this.nudSoLuongAnhTo.Location = new System.Drawing.Point(221, 29);
            this.nudSoLuongAnhTo.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.nudSoLuongAnhTo.Name = "nudSoLuongAnhTo";
            this.nudSoLuongAnhTo.Size = new System.Drawing.Size(43, 23);
            this.nudSoLuongAnhTo.TabIndex = 6;
            // 
            // ckbSendAnh
            // 
            this.ckbSendAnh.AutoSize = true;
            this.ckbSendAnh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ckbSendAnh.Location = new System.Drawing.Point(12, 236);
            this.ckbSendAnh.Name = "ckbSendAnh";
            this.ckbSendAnh.Size = new System.Drawing.Size(71, 20);
            this.ckbSendAnh.TabIndex = 181;
            this.ckbSendAnh.Text = "Gửi ảnh";
            this.ckbSendAnh.UseVisualStyleBackColor = true;
            this.ckbSendAnh.CheckedChanged += new System.EventHandler(this.ckbSendAnh_CheckedChanged);
            // 
            // ckbNhanTinVanBan
            // 
            this.ckbNhanTinVanBan.AutoSize = true;
            this.ckbNhanTinVanBan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ckbNhanTinVanBan.Location = new System.Drawing.Point(12, 12);
            this.ckbNhanTinVanBan.Name = "ckbNhanTinVanBan";
            this.ckbNhanTinVanBan.Size = new System.Drawing.Size(123, 20);
            this.ckbNhanTinVanBan.TabIndex = 179;
            this.ckbNhanTinVanBan.Text = "Nhắn tin văn bản";
            this.ckbNhanTinVanBan.UseVisualStyleBackColor = true;
            this.ckbNhanTinVanBan.CheckedChanged += new System.EventHandler(this.ckbNhanTinVanBan_CheckedChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 325);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(156, 16);
            this.label11.TabIndex = 173;
            this.label11.Text = "Số lượng người cần nhắn:";
            this.label11.Visible = false;
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 354);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(159, 16);
            this.label8.TabIndex = 170;
            this.label8.Text = "Thời gian delay/lượt nhắn:";
            // 
            // ckbInboxAll
            // 
            this.ckbInboxAll.AutoSize = true;
            this.ckbInboxAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ckbInboxAll.Location = new System.Drawing.Point(365, 325);
            this.ckbInboxAll.Name = "ckbInboxAll";
            this.ckbInboxAll.Size = new System.Drawing.Size(103, 20);
            this.ckbInboxAll.TabIndex = 178;
            this.ckbInboxAll.Text = "Nhắn toàn bộ";
            this.ckbInboxAll.UseVisualStyleBackColor = true;
            this.ckbInboxAll.Visible = false;
            this.ckbInboxAll.CheckedChanged += new System.EventHandler(this.ckbInboxAll_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(228, 354);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 16);
            this.label10.TabIndex = 172;
            this.label10.Text = "đến";
            // 
            // nudDelayTo
            // 
            this.nudDelayTo.Location = new System.Drawing.Point(259, 352);
            this.nudDelayTo.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudDelayTo.Name = "nudDelayTo";
            this.nudDelayTo.Size = new System.Drawing.Size(56, 23);
            this.nudDelayTo.TabIndex = 169;
            this.nudDelayTo.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudCountInviteTo
            // 
            this.nudCountInviteTo.Location = new System.Drawing.Point(259, 324);
            this.nudCountInviteTo.Maximum = new decimal(new int[] {
            1874919423,
            2328306,
            0,
            0});
            this.nudCountInviteTo.Name = "nudCountInviteTo";
            this.nudCountInviteTo.Size = new System.Drawing.Size(56, 23);
            this.nudCountInviteTo.TabIndex = 176;
            this.nudCountInviteTo.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudCountInviteTo.Visible = false;
            this.nudCountInviteTo.ValueChanged += new System.EventHandler(this.nudCountInviteTo_ValueChanged);
            // 
            // nudCountInviteFrom
            // 
            this.nudCountInviteFrom.Location = new System.Drawing.Point(169, 323);
            this.nudCountInviteFrom.Maximum = new decimal(new int[] {
            1874919423,
            2328306,
            0,
            0});
            this.nudCountInviteFrom.Name = "nudCountInviteFrom";
            this.nudCountInviteFrom.Size = new System.Drawing.Size(56, 23);
            this.nudCountInviteFrom.TabIndex = 177;
            this.nudCountInviteFrom.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudCountInviteFrom.Visible = false;
            this.nudCountInviteFrom.ValueChanged += new System.EventHandler(this.nudCountInviteFrom_ValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(319, 354);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(31, 16);
            this.label12.TabIndex = 171;
            this.label12.Text = "giây";
            // 
            // nudDelayFrom
            // 
            this.nudDelayFrom.Location = new System.Drawing.Point(169, 352);
            this.nudDelayFrom.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudDelayFrom.Name = "nudDelayFrom";
            this.nudDelayFrom.Size = new System.Drawing.Size(56, 23);
            this.nudDelayFrom.TabIndex = 168;
            this.nudDelayFrom.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(319, 326);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 16);
            this.label1.TabIndex = 174;
            this.label1.Text = "người";
            this.label1.Visible = false;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(228, 326);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 16);
            this.label2.TabIndex = 175;
            this.label2.Text = "đến";
            this.label2.Visible = false;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(120)))), ((int)(((byte)(229)))));
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Location = new System.Drawing.Point(118, 407);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(107, 39);
            this.btnAdd.TabIndex = 183;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Maroon;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnClose.Location = new System.Drawing.Point(243, 407);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(107, 39);
            this.btnClose.TabIndex = 184;
            this.btnClose.Text = "Đóng";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // fCauHinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 458);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.plComment);
            this.Controls.Add(this.plAnh);
            this.Controls.Add(this.ckbSendAnh);
            this.Controls.Add(this.ckbNhanTinVanBan);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.ckbInboxAll);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.nudDelayTo);
            this.Controls.Add(this.nudCountInviteTo);
            this.Controls.Add(this.nudCountInviteFrom);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.nudDelayFrom);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "fCauHinh";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cấu hình chạy";
            this.Load += new System.EventHandler(this.fCauHinh_Load);
            this.plComment.ResumeLayout(false);
            this.plComment.PerformLayout();
            this.plAnh.ResumeLayout(false);
            this.plAnh.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSoLuongAnhFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSoLuongAnhTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDelayTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCountInviteTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCountInviteFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDelayFrom)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel plComment;
        private System.Windows.Forms.RichTextBox txtNoiDung;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel plAnh;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtAnh;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown nudSoLuongAnhFrom;
        private System.Windows.Forms.NumericUpDown nudSoLuongAnhTo;
        private System.Windows.Forms.CheckBox ckbSendAnh;
        private System.Windows.Forms.CheckBox ckbNhanTinVanBan;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox ckbInboxAll;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown nudDelayTo;
        private System.Windows.Forms.NumericUpDown nudCountInviteTo;
        private System.Windows.Forms.NumericUpDown nudCountInviteFrom;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown nudDelayFrom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnClose;
    }
}