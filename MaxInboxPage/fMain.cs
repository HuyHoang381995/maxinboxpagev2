﻿using HttpRequest;
using maxcare.Properties;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Net.WebRequestMethods;
using File = System.IO.File;
using System.Web.Script.Serialization;
using System.Diagnostics;
using MCommon;
using Common;
using DeviceId;
using License.RNCryptor;
using System.Drawing.Imaging;

namespace maxcare
{
    public partial class fMain : Form
    {
        Random rd = new Random();
        bool isStop = false;

        public fMain(string log)
        {
            //CheckKey.CheckVersion(nameSoft);
            InitializeComponent();
            settings = new JSON_Settings("config");
            LoadSettings();
            //if (Base.isCalledByOtherProgram)
            //{
            //    if (File.Exists(@"chromedriver.exe"))
            //    {
            //        try
            //        {
            //            File.Delete(Base.currentPath + "chromedriver.exe");
            //            File.Copy(@"chromedriver.exe", Base.currentPath + "chromedriver.exe");
            //        }
            //        catch
            //        {
            //        }
            //    }
            //    statusStrip1.Visible = false;
            //}
            //else
            //{
            //    string[] dt = log.Split('|');
            //    lblDateExpried.Text = Convert.ToDateTime(dt[2]).ToString("dd/MM/yyyy");
            //    lblKey.Text = dt[3].Substring(0, 10) + "****";
            //    lblUser.Text = dt[0];
            //}
        }

        void LoadSettings()
        {
            txtCookie.Text = settings.GetValue("cookie");
            rbNu.Checked = settings.GetValueBool("rbNu");
            rbNam.Checked = settings.GetValueBool("rbNam");
            rbAllGender.Checked = settings.GetValueBool("rbAllGender");
            ckbAllTime.Checked = settings.GetValueBool("ckbAllTime");
            gbTimeFind.Enabled = !ckbAllTime.Checked;
        }
        void SaveSettings()
        {
            settings.Update("cookie", txtCookie.Text.Trim());
            settings.Update("rbAllGender", rbAllGender.Checked);
            settings.Update("rbNam", rbNam.Checked);
            settings.Update("rbNu", rbNu.Checked);
            settings.Update("ckbAllTime", ckbAllTime.Checked);
            settings.Save();
        }
        public const int softIndex = 19;
        public const string nameSoft = "maxinvitelikepagev2";
        public string deviceId = "";

        JSON_Settings settings;


        #region Close, Min, Max Form

        private void Button1_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
            }
            else
            {
                this.WindowState = FormWindowState.Maximized;
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        #endregion

        protected override void OnLoad(EventArgs args)
        {
            Application.Idle += this.OnLoaded;
        }
        private void OnLoaded(object sender, EventArgs e)
        {
            Application.Idle -= this.OnLoaded;

            //if (!Base.isCalledByOtherProgram)
            //    LoadCheckTool();
            UpdateSTTOnDgv();
            LoadSettings();
        }
        void DelayThaoTacNho(int delay = 0)
        {
            MCommon.Common.DelayTime(rd.Next(1 + delay, 4 + delay));
        }
        private List<string> CloneList(List<string> lstFrom)
        {
            List<string> lstOutput = new List<string>();
            try
            {
                for (int i = 0; i < lstFrom.Count; i++)
                {
                    lstOutput.Add(lstFrom[i]);
                }
            }
            catch
            {

            }
            return lstOutput;
        }
        bool RepTinNhanPage(Chrome chrome, string idPage, int count, bool isSendMess, List<string> lstTinNhan, bool isSenanh, List<string> lstImg, int delayFrom, int delayTo, int countPicFrom = 0, int countPicTo = 0, bool isInboxAll = false)
        {
            bool check = true;
            string content = "";
            string pathPic = "";
            int countPicBefore = 0;
            int countPicLoaded = 0;
            int countPic = 0;
            List<string> lstMess = CloneList(lstTinNhan);
            List<string> lstPic = new List<string>();

            try
            {
                for (int i = 0; i < count; i++)
                {
                    lstPic = CloneList(lstImg);
                    chrome.GotoURL("https://m.facebook.com/messages/?pageID=" + idPage);
                    DelayThaoTacNho(2);
                    countPicLoaded = 0;
                    if (isInboxAll)
                    {
                        count = Convert.ToInt32(chrome.ExecuteScript("return document.querySelectorAll('#threadlist_rows a.touchable').length"));
                    }
                    if (chrome.CheckExistElementv2("document.querySelectorAll('#threadlist_rows a.touchable')[" + i + "]", 5))
                    {
                        if (i > 6)
                        {
                            chrome.ScrollSmooth("document.querySelectorAll('#threadlist_rows a.touchable')[" + i + "]");
                            DelayThaoTacNho();
                        }
                        if (isSendMess)
                        {
                            content = lstMess[rd.Next(0, lstMess.Count())];
                            lstMess.Remove(content);
                            if (lstMess.Count() == 0)
                            {
                                lstMess = CloneList(lstTinNhan);
                            }
                        }

                        chrome.ExecuteScript("document.querySelectorAll('#threadlist_rows a.touchable')[" + i + "].click()");
                        DelayThaoTacNho(2);
                        //gui anh
                        if (isSenanh)
                        {
                            if (chrome.CheckExistElement("[data-sigil=\"m-raw-file-input\"]", 10))
                            {
                                countPic = rd.Next(countPicFrom, countPicTo + 1);
                                for (int j = 0; j < countPic; j++)
                                {
                                    countPicBefore = Convert.ToInt32(chrome.ExecuteScript("return document.querySelectorAll('[role=\"presentation\"] img').length"));
                                    pathPic = lstPic[rd.Next(0, lstPic.Count())];
                                    lstPic.Remove(pathPic);
                                    if (lstPic.Count() == 0)
                                    {
                                        lstPic = CloneList(lstImg);
                                    }
                                    chrome.SendKeys(4, "[data-sigil=\"m-raw-file-input\"]", pathPic);
                                    DelayThaoTacNho();
                                recheck:
                                    countPicLoaded = Convert.ToInt32(chrome.ExecuteScript("return document.querySelectorAll('[role=\"presentation\"] img').length"));
                                    if (countPicLoaded <= countPicBefore)
                                    {
                                        goto recheck;
                                    }
                                }
                            }
                        }
                        //gui tin nhan
                        if (isSendMess)
                        {
                            if (chrome.CheckExistElement("textarea", 5))
                            {
                                chrome.SendKeys(4, "textarea", content, 0.08);
                                DelayThaoTacNho(2);
                            }
                        }
                        chrome.Click(4, "[data-sigil=\"m-messaging-button\"]");
                        DelayThaoTacNho(2);
                        chrome.DelayTime(rd.Next(delayFrom, delayTo + 1));
                    }
                    else
                    {
                        break;
                    }
                }
            }
            catch
            {

                check = false;
            }
            return check;
        }
        int LoadNguoiDaNhanTinPage(Chrome chrome, string idPage)
        {
            int output = 1;
            try
            {
                chrome.GotoURL("https://m.facebook.com/messages/?pageID=" + idPage);
                DelayThaoTacNho(2);

            }
            catch
            {
                output = 0;
            }
            return output;
        }
        private void LoadCheckTool()
        {
            deviceId = CommonCSharp.Md5Encode(new DeviceIdBuilder().AddMachineName().AddProcessorId().AddMotherboardSerialNumber().AddSystemDriveSerialNumber().ToString());
            try
            {
                string userName = ""; string pass = "";
                userName = Settings.Default.UserName;
                pass = Settings.Default.PassWord;

                if (userName == "" || pass == "" || CommonCSharp.IsValidMail(userName) == false)
                {
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        this.Hide();
                    });
                    fActive fa = new fActive(0, deviceId);
                    fa.ShowInTaskbar = true;
                    fa.ShowDialog();
                    return;
                }

                RequestXNet request = new RequestXNet("", "", "", 0);
                string api_token = CommonCSharp.ReadHTMLCode("http://app.minsoftware.vn/api/auth?datavery=" + CommonCSharp.Base64Encode(userName + "|" + pass)).Replace("\"", "");
                if (api_token.Trim() == "")
                {
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        this.Hide();
                    });
                    fActive fa = new fActive(0, deviceId);
                    fa.ShowInTaskbar = true;
                    fa.ShowDialog();
                    return;
                }

                Random rd = new Random();
                int codekeyrandom = rd.Next(100000, 999999);
                string url = "http://app.minsoftware.vn/minapi/minapi/api.php/Check?data=";
                string strRequest = deviceId + "|" + api_token + "|" + fMain.softIndex + "|" + codekeyrandom + "|" + "minsoftware0803";

                Encryptor encrypt = new Encryptor();
                string strEncryptRequest = encrypt.Encrypt(strRequest, "thangtungmin080394");

                string checkLisence = request.RequestGet(url + strEncryptRequest).Replace("\"", "");
                checkLisence = CommonCSharp.Base64Decode(checkLisence);

                Decryptor decrypt = new Decryptor();
                checkLisence = decrypt.Decrypt(checkLisence, "thangtungmin080394");
                if (checkLisence.Contains("chuakichhoat") || checkLisence.Contains("error") || checkLisence.Contains("hethan"))
                {
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        this.Hide();
                    });
                    fActive fa = new fActive(0, deviceId);
                    fa.ShowInTaskbar = true;
                    fa.ShowDialog();
                    return;
                }

                string full_name = checkLisence.Split('|')[0];
                string api_token_sv = checkLisence.Split('|')[1];
                string date_exp = checkLisence.Split('|')[2];
                string device_server = checkLisence.Split('|')[3];
                string codekeyrandom_sv = checkLisence.Split('|')[4];
                string keystatic_sv = checkLisence.Split('|')[5];

                if (deviceId != device_server || api_token_sv != api_token || codekeyrandom_sv != codekeyrandom.ToString() || keystatic_sv != "minsoftware0803")
                {
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        this.Hide();
                    });
                    fActive fa = new fActive(0, deviceId);
                    fa.ShowInTaskbar = true;
                    fa.ShowDialog();
                    return;
                }
                else
                {
                    lblStatus.Text = "Đã kích hoạt";
                }
            }
            catch
            {
                this.Invoke((MethodInvoker)delegate ()
                {
                    this.Hide();
                });
                fActive fa = new fActive(0, deviceId);
                fa.ShowInTaskbar = true;
                fa.ShowDialog();
                return;
            }
        }
        private void rControl(string dt)
        {
            if (dt == "start")
            {
                this.Invoke((MethodInvoker)delegate ()
                {
                    btnPause.Enabled = true;
                    btnStart.Enabled = false;
                });
            }
            else if (dt == "stop")
            {
                this.Invoke((MethodInvoker)delegate ()
                {
                    btnPause.Text = "Tạm dừng";
                    btnPause.Enabled = false;
                    btnStart.Enabled = true;
                });
            }
        }


        object k = new object();

        private void dtgvAcc_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyValue == (int)System.Windows.Forms.Keys.Space)
                {
                    for (int i = 0; i < dgvPage.SelectedRows.Count; i++)
                    {
                        int a = dgvPage.SelectedRows[i].Index;
                        dgvPage.Rows[a].Cells["cChosePage"].Value = !Convert.ToBoolean(dgvPage.Rows[a].Cells["cChosePage"].Value);
                    }
                }
            }
            catch { }
        }

        void UpdateSTTOnDgv()
        {
            for (int i = 0; i < dgvPage.RowCount; i++)
            {

                MCommon.Common.SetStatusDataGridView(dgvPage, i, "cSTT", i + 1);
            }
        }

        private void chọnTấtCảToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < dgvPage.Rows.Count; i++)
                    MCommon.Common.SetStatusDataGridView(dgvPage, i, "cChosePage", true);
            }
            catch
            { }
        }

        private void bỏChọnTấtCảToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < dgvPage.Rows.Count; i++)
                    MCommon.Common.SetStatusDataGridView(dgvPage, i, "cChosePage", false);
            }
            catch
            { }
        }


        private void xóaToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < dgvPage.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(MCommon.Common.GetStatusDataGridView(dgvPage, i, "cChosePage")))
                    {
                        dgvPage.Rows.RemoveAt(i--);
                    }
                }
                UpdateSTTOnDgv(dgvPage, "cSTT");
            }
            catch (Exception)
            { }
        }
        private void LoadStatus(string content)
        {
            this.Invoke(new Action(delegate ()
            {
                lblTrangthai.Text = content;
            }));
        }

        private void btnShare_Click_1(object sender, EventArgs e)
        {
            try
            {
                string cookie = txtCookie.Text.Trim();
                rControl("start");
                LoadStatus("Check cookie...");
                if (!MCommon.CommonRequest.CheckLiveCookie(cookie))
                {
                    MCommon.Common.ShowMessageBox("Cookie Die!", 2);
                    LoadStatus("Cookie Die!");
                    rControl("stop");
                    return;
                }
                #region input
                string uid = Regex.Match(cookie + ";", "c_user=(.*?);").Groups[1].Value;
                JSON_Settings settings = new JSON_Settings("CauHinhNhanTin");
                int soluongFrom = settings.GetValueInt("nudCountInvite");
                int soluongTo = settings.GetValueInt("nudCountInviteTo");
                if (soluongFrom > soluongTo)
                    soluongTo = soluongFrom;
                int count = rd.Next(soluongFrom, soluongTo + 1);
                int delayFrom = settings.GetValueInt("nudDelayFrom");
                int delayTo = settings.GetValueInt("nudDelayTo");
                if (delayFrom > delayTo)
                    delayTo = delayFrom;
                int countPicFrom = settings.GetValueInt("nudSoLuongAnhFrom");
                int countPicTo = settings.GetValueInt("nudSoLuongAnhTo");
                int countPic = rd.Next(countPicFrom, countPicTo + 1);
                bool isInviteAll = settings.GetValueBool("ckbInboxAll");
                bool isSendMess = settings.GetValueBool("ckbNhanTinVanBan");
                bool isSendPic = settings.GetValueBool("ckbSendAnh");
                bool isInboxAll = settings.GetValueBool("ckbInboxAll");
                List<string> lstContentMess = new List<string>();
                List<string> lstPicture = new List<string>();
                List<string> lstIDpage = new List<string>();
                List<string> lstUIDKhach = new List<string>();
                string pageID = "";
                if (isSendMess)
                {
                    lstContentMess = settings.GetValueList("txtNoiDung");
                }
                if (isSendPic)
                {
                    string urlFileAnh = settings.GetValue("txtAnh");
                    var files = Directory.GetFiles(urlFileAnh, "*.*", SearchOption.AllDirectories);
                    foreach (string filename in files)
                    {
                        if (Regex.IsMatch(filename, @".jpg|.png|.gif$"))
                            lstPicture.Add(filename);
                    }
                }
                for (int i = 0; i < dgvKhachDaNhanTin.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(dgvKhachDaNhanTin.Rows[i].Cells["cChoseUID"].Value) == true)
                    {
                        lstUIDKhach.Add(dgvKhachDaNhanTin.Rows[i].Cells["cUID"].Value.ToString());
                        lstIDpage.Add(dgvKhachDaNhanTin.Rows[i].Cells["cIdPageOfCus"].Value.ToString());
                    }
                }
                #endregion
                if (lstUIDKhach.Count > 0)
                {
                    new Thread(() =>
                    {
                        Chrome chrome = null;
                        string content = "";
                        string pathPic = "";
                        int countPicBefore = 0;
                        int countPicLoaded = 0;
                        int checkLoadPic = 0;
                        List<string> lstMess = CloneList(lstContentMess);
                        List<string> lstPicClone = CloneList(lstPicture);
                        try
                        {
                            #region Đăng nhập
                            LoadStatus("Đang mở trình duyệt...");

                            if (isStop)
                                goto Xong;

                            Point SizeChrome = MCommon.Common.GetSizeChrome(3, 1);
                            bool isSuccess = false;
                            try
                            {
                                chrome = new Chrome()
                                {
                                    DisableImage = false,
                                    Size = SizeChrome,
                                    TimeWaitForSearchingElement = 3,
                                    TimeWaitForLoadingPage = 60,
                                    App = "https://m.facebook.com/",
                                };

                                string sLinkProfile = Base.profilePath + "\\" + uid;
                                if (Base.profilePath != "" && Directory.Exists(sLinkProfile))
                                {
                                    chrome.ProfilePath = Base.profilePath;
                                }

                                if (!chrome.Open())
                                {
                                    LoadStatus("Lỗi mở trình duyệt!");
                                    goto Xong;
                                }

                                LoadStatus("Đang đăng nhập...");
                                if (!isSuccess)
                                    isSuccess = CommonChrome.LoginFacebookUsingCookie(chrome, cookie, "https://m.facebook.com/");

                                if (!isSuccess)
                                {
                                    LoadStatus("Đăng nhập thất bại!");
                                    goto Xong;
                                }

                                if (chrome.CheckExistElement("button[value=\"OK\"]"))
                                {
                                    chrome.Click(4, "button[value=\"OK\"]");
                                    chrome.DelayTime(1);
                                }
                                if (chrome.GetURL().StartsWith("https://m.facebook.com/gettingstarted/"))
                                {
                                    while (chrome.CheckExistElement("#nux-nav-button", 3))
                                    {
                                        chrome.Click(1, "nux-nav-button");
                                        if (chrome.CheckExistElement("#qf_skip_dialog_skip_link", 3))
                                        {
                                            chrome.Click(1, "qf_skip_dialog_skip_link");
                                            chrome.DelayTime(3);
                                        }
                                    }
                                }

                                LoadStatus("Đăng nhập thành công!");
                            }
                            catch (Exception ex)
                            {
                                LoadStatus("Đăng nhập thất bại!");
                                goto Xong;
                            }
                            #endregion

                            if (isStop)
                                goto Xong;

                            LoadStatus("Bắt đầu rep inbox page...");

                            ///Begin Code here
                            for (int i = 0; i < lstUIDKhach.Count; i++)
                            {
                                pageID = lstIDpage[i].ToString();
                                if (isSendMess)
                                {
                                    content = lstMess[rd.Next(0, lstMess.Count())];
                                    lstMess.Remove(content);
                                    if (lstMess.Count() == 0)
                                    {
                                        lstMess = CloneList(lstContentMess);
                                    }
                                }
                                string link = "https://m.facebook.com/messages/read/?tid=cid.c." + lstUIDKhach[i] + "%3A" + pageID + "&pageID=" + pageID + "&entrypoint=web%3Atrigger%3Athread_list_thread&surface_hierarchy=unknown&paipv=1#fua";
                                //vao trang tin nhan
                                chrome.GotoURL(link);
                                chrome.DelayTime(5);
                                //gui anh
                                if (isSendPic)
                                {
                                    checkLoadPic = 0;
                                    if (chrome.CheckExistElement("[data-sigil=\"m-raw-file-input\"]", 10))
                                    {
                                        countPic = rd.Next(countPicFrom, countPicTo + 1);
                                        countPicBefore = Convert.ToInt32(chrome.ExecuteScript("return document.querySelectorAll('[role=\"presentation\"] img').length"));

                                        for (int j = 0; j < countPic; j++)
                                        {
                                            pathPic = lstPicClone[rd.Next(0, lstPicClone.Count())];
                                            lstPicClone.Remove(pathPic);
                                            if (lstPicClone.Count() == 0)
                                            {
                                                lstPicClone = CloneList(lstPicture);
                                            }
                                            chrome.SendKeys(4, "[data-sigil=\"m-raw-file-input\"]", pathPic);
                                            DelayThaoTacNho();
                                        }
                                    recheck:
                                        countPicLoaded = Convert.ToInt32(chrome.ExecuteScript("return document.querySelectorAll('[role=\"presentation\"] img').length"));
                                        if (countPicLoaded <= countPicBefore)
                                        {
                                            checkLoadPic++;
                                            chrome.DelayTime(1);
                                            if (checkLoadPic > 30)
                                            {
                                                goto sendmess;
                                            }
                                            goto recheck;
                                        }
                                    }
                                }
                            //gui tin nhan
                            sendmess:
                                if (isSendMess)
                                {
                                    if (chrome.CheckExistElement("textarea", 5))
                                    {
                                        chrome.SendKeys(4, "textarea", content, 0.08);
                                        DelayThaoTacNho(2);
                                    }
                                }
                                chrome.Click(4, "[data-sigil=\"m-messaging-button\"]");
                                DelayThaoTacNho(2);
                                for (int j = 0; j < dgvKhachDaNhanTin.Rows.Count; j++)
                                {
                                    if (dgvKhachDaNhanTin.Rows[j].Cells["cUID"].Value.ToString() == lstUIDKhach[i])
                                    {
                                        UpdateCellOnDgv(dgvKhachDaNhanTin, j, "cState", "Đã nhắn tin");
                                    }
                                }
                                chrome.DelayTime(rd.Next(delayFrom, delayTo + 1));
                            }
                            ///End Code here

                            LoadStatus("Đã thực hiện xong!");
                        }
                        catch (Exception ex)
                        {
                            LoadStatus("Lỗi không xác định!");
                            MCommon.Common.ExportError(chrome, ex);
                        }

                    Xong:
                        try
                        {
                            chrome.Close();
                        }
                        catch
                        {
                        }

                        if (isStop)
                            LoadStatus("Đã dừng!");

                        rControl("stop");
                    }).Start();
                }
                else
                {
                    LoadStatus("Chưa chọn danh sách khách hàng!");
                    rControl("stop");
                }
            }
            catch (Exception ex)
            {
                LoadStatus("Lỗi không xác định!");
                rControl("stop");
                MCommon.Common.ExportError(null, ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chrome"></param>
        /// <returns>1-checkpoint,2-chrome closed</returns>
        int CheckStatusChrome(Chrome chrome, bool isCheckLiveCookie = true, bool isCheckChromeClosed = true)
        {
            int status = 0;
            if (isCheckChromeClosed && chrome.CheckChromeClosed())
                status = 2;
            else if (isCheckLiveCookie && !CommonChrome.CheckLiveCookie(chrome))
                status = 1;
            return status;
        }

        private void btnPause_Click_1(object sender, EventArgs e)
        {
            try
            {
                isStop = true;
                btnPause.Enabled = false;
                btnPause.Text = "Đang dừng...";
            }
            catch { }
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            SaveSettings();
            try
            {
                Environment.Exit(0);
            }
            catch
            {
                this.Close();
            }
        }

        private void toolStripStatusLabel9_Click(object sender, EventArgs e)
        {
            this.Hide();
            Settings.Default.UserName = "";
            Settings.Default.PassWord = "";
            Settings.Default.Save();
            fActive fa = new fActive(0, deviceId);
            fa.ShowInTaskbar = true;
            fa.ShowDialog();
        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
        // Mời thì sử dụng trình duyệt
        private void Button3_Click(object sender, EventArgs e)
        {
            dgvKhachDaNhanTin.Rows.Clear();
            LoadStatus("Check cookie...");
            if (!MCommon.CommonRequest.CheckLiveCookie(txtCookie.Text))
            {
                MCommon.Common.ShowMessageBox("Cookie Die!", 2);
                LoadStatus("Cookie Die!");
                rControl("stop");
                return;
            }
            LoadStatus("Đang load danh sách page...");
            dgvPage.Rows.Clear();
            string token = CommonChrome.GetTokenEAAAAZrequest(txtCookie.Text);
            RequestXNet http = new RequestXNet(txtCookie.Text);
            string html = http.RequestGet("https://graph.facebook.com/me/accounts?fields=id,name,likes.summary(true)&access_token=" + token);
            //string html = http.RequestGet("https://graph.facebook.com/me/accounts?access_token=" + token);
            JObject json = JObject.Parse(html);
            foreach (var item in json["data"])
                dgvPage.Rows.Add(false, "", item["id"].ToString(), item["name"].ToString(), item["likes"].ToString());
            UpdateSTTOnDgv(dgvPage, "cSTT");
            LoadStatus("Đã load xong danh sách page!");
        }

        void UpdateSTTOnDgv(DataGridView dgv, string columnName)
        {
            for (int i = 0; i < dgv.RowCount; i++)
            {
                MCommon.Common.SetStatusDataGridView(dgv, i, columnName, i + 1);
            }
        }
        void UpdateCellOnDgv(DataGridView dgv, int row, string columnName, string content)
        {
            MCommon.Common.SetStatusDataGridView(dgv, row, columnName, content);
        }
        private void DtgvAcc_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                    dgvPage.CurrentRow.Cells["cChosePage"].Value = !Convert.ToBoolean(dgvPage.CurrentRow.Cells["cChosePage"].Value);
            }
            catch
            {
            }
        }

        private void FMain_Load(object sender, EventArgs e)
        {
            LoadSettings();
        }

        private void chonBôiĐenToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                var rowSelect = dgvPage.SelectedRows;
                for (int i = 0; i < rowSelect.Count; i++)
                    MCommon.Common.SetStatusDataGridView(dgvPage, rowSelect[i].Index, "cChosePage", true);
            }
            catch
            {
            }
        }

        private void dgvPage_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            if (e.Column.Name == "cSTT" || e.Column.Name == "cLike")
            {
                e.SortResult = int.Parse(e.CellValue1.ToString() == "" ? "-1" : e.CellValue1.ToString()).CompareTo(int.Parse(e.CellValue2.ToString() == "" ? "-1" : e.CellValue2.ToString()));
                e.Handled = true;//pass by the default sorting
                UpdateSTTOnDgv(dgvPage, "cSTT");
            }
        }

        private void btnCauHinh_Click(object sender, EventArgs e)
        {
            new fCauHinh().ShowDialog();
        }

        private void loadNgườiĐãNhắnTinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgvKhachDaNhanTin.Rows.Clear();
            string cookie = txtCookie.Text.Trim();
            rControl("start");
            //LoadStatus("Check cookie...");
            //if (!MCommon.CommonRequest.CheckLiveCookie(cookie))
            //{
            //    MCommon.Common.ShowMessageBox("Cookie Die!", 2);
            //    LoadStatus("Cookie Die!");
            //    rControl("stop");
            //    return;
            //}
            //string uid = Regex.Match(cookie + ";", "c_user=(.*?);").Groups[1].Value;
            List<string> lstIDPage = new List<string>();
            List<string> lstData = new List<string>();
            for (int i = 0; i < dgvPage.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dgvPage.Rows[i].Cells["cChosePage"].Value) == true)
                {
                    lstIDPage.Add(dgvPage.Rows[i].Cells["cIdPage"].Value.ToString());
                }
            }
            string token = "";
            LoadStatus("Đang load danh sách người đã nhắn tin...");
            for (int i = 0; i < lstIDPage.Count(); i++)
            {
                lstData = GetListPeople(txtCookie.Text, lstIDPage[i]);

            }
            UpdateSTTOnDgv(dgvKhachDaNhanTin, "cSTTUID");
            LoadStatus("Đã load xong danh sách người đã nhắn tin...");
            rControl("stop");
        }
        public List<string> GetListPeople(string cookie, string page_id)
        {
            List<string> lstUid = new List<string>();
            try
            {
                RequestXNet request = new RequestXNet(cookie, "", "", 0);
                string uid = Regex.Match(cookie, "c_user=(.*?);").Groups[1].Value;
                string html = request.RequestGet("https://m.facebook.com/composer/ocelot/async_loader/?publisher=feed");
                string fb_dtsg = Regex.Match(html, @"fb_dtsg\\"" value=\\""(.*?)\\"" ").Groups[1].Value;
                string data = "batch_name=MessengerGraphQLThreadlistFetcher&__user=" + uid + "&fb_dtsg=" + fb_dtsg + "&av=" + page_id + "&queries={\"o0\":{\"doc_id\":\"3566388080113165\",\"query_params\":{\"limit\":200,\"before\":null,\"tags\":[\"INBOX\"],\"isWorkUser\":false,\"includeDeliveryReceipts\":true,\"includeSeqID\":false,\"is_work_teamwork_not_putting_muted_in_unreads\":false}}}";
                html = request.RequestPost("https://www.facebook.com/api/graphqlbatch/", data);
                if (html.Contains("{\"successful_results\":1,\"error_results\":0,\"skipped_results\":0}"))
                {
                    html = html.Replace("{\"successful_results\":1,\"error_results\":0,\"skipped_results\":0}", "");
                    JObject json = JObject.Parse(html);
                    int dem = json["o0"]["data"]["viewer"]["message_threads"]["nodes"].Count();
                    if (dem > 0)
                    {
                        for (int i = 0; i < dem; i++)
                        {
                            if (json["o0"]["data"]["viewer"]["message_threads"]["nodes"][i]["all_participants"]["edges"][0]["node"]["messaging_actor"]["name"].ToString() != "")
                            {
                                lstUid.Add(json["o0"]["data"]["viewer"]["message_threads"]["nodes"][i]["all_participants"]["edges"][0]["node"]["messaging_actor"]["id"].ToString() + "|" + json["o0"]["data"]["viewer"]["message_threads"]["nodes"][i]["all_participants"]["edges"][0]["node"]["messaging_actor"]["name"].ToString() + "|" + json["o0"]["data"]["viewer"]["message_threads"]["nodes"][i]["all_participants"]["edges"][0]["node"]["messaging_actor"]["gender"].ToString() + "|" + page_id + "|" + json["o0"]["data"]["viewer"]["message_threads"]["nodes"][i]["updated_time_precise"].ToString()+"|"+ json["o0"]["data"]["viewer"]["message_threads"]["nodes"][i]["is_viewer_subscribed"].ToString());
                            }
                        }
                        data = "batch_name=MessengerGraphQLThreadlistFetcher&__user=" + uid + "&fb_dtsg=" + fb_dtsg + "&av=112502927589745&queries={\"o0\":{\"doc_id\":\"3566388080113165\",\"query_params\":{\"limit\":200,\"before\": 1620958341186,\"tags\":[\"INBOX\"],\"isWorkUser\":false,\"includeDeliveryReceipts\":true,\"includeSeqID\":false,\"is_work_teamwork_not_putting_muted_in_unreads\":false}}}";
                    }
                    else
                        goto Xong;
                }

            }
            catch
            {

            }
        Xong:
            return lstUid;
        }
        private void dgvKhachDaNhanTin_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                    dgvKhachDaNhanTin.CurrentRow.Cells["cChoseUID"].Value = !Convert.ToBoolean(dgvKhachDaNhanTin.CurrentRow.Cells["cChoseUID"].Value);
            }
            catch
            {
            }
        }

        private void chọnBôiĐenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var rowSelect = dgvKhachDaNhanTin.SelectedRows;
                for (int i = 0; i < rowSelect.Count; i++)
                    MCommon.Common.SetStatusDataGridView(dgvKhachDaNhanTin, rowSelect[i].Index, "cChoseUID", true);
            }
            catch
            {
            }
        }

        private void chọnTâtCảToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var rowSelect = dgvKhachDaNhanTin.Rows;
                for (int i = 0; i < rowSelect.Count; i++)
                    MCommon.Common.SetStatusDataGridView(dgvKhachDaNhanTin, rowSelect[i].Index, "cChoseUID", true);
            }
            catch
            {
            }
        }

        private void bỏChọnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var rowSelect = dgvKhachDaNhanTin.SelectedRows;
                for (int i = 0; i < rowSelect.Count; i++)
                    MCommon.Common.SetStatusDataGridView(dgvKhachDaNhanTin, rowSelect[i].Index, "cChoseUID", false);
            }
            catch
            {
            }
        }

        private void bỏChọnTấtCảToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                var rowSelect = dgvKhachDaNhanTin.Rows;
                for (int i = 0; i < rowSelect.Count; i++)
                    MCommon.Common.SetStatusDataGridView(dgvKhachDaNhanTin, rowSelect[i].Index, "cChoseUID", false);
            }
            catch
            {
            }
        }

        private void btnQuyetInbox_Click(object sender, EventArgs e)
        {
            dgvKhachDaNhanTin.Rows.Clear();
            string cookie = txtCookie.Text.Trim();
            rControl("start");
            List<string> lstIDPage = new List<string>();
            List<string> lstData = new List<string>();
            List<string> lstDataDgv = new List<string>();
            long timeStart = ((DateTimeOffset)(Convert.ToDateTime(dtpStart.Value.ToString()))).ToUnixTimeSeconds();
            long timeEnd = ((DateTimeOffset)(Convert.ToDateTime(dtpEnd.Value.ToString()))).ToUnixTimeSeconds();
            long timeMess = 0;
            for (int i = 0; i < dgvPage.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dgvPage.Rows[i].Cells["cChosePage"].Value) == true)
                {
                    lstIDPage.Add(dgvPage.Rows[i].Cells["cIdPage"].Value.ToString());
                }
            }
            LoadStatus("Đang load danh sách người đã nhắn tin...");
            for (int i = 0; i < lstIDPage.Count(); i++)
            {
                lstData = GetListPeople(txtCookie.Text, lstIDPage[i]);

                #region loc goi tinh
                for (int j = 0; j < lstData.Count; j++)
                {
                    if (rbNam.Checked && lstData[j].Split('|')[2].ToString() != "MALE")
                    {
                        lstData[j] = "";
                    }
                    else if (rbNu.Checked && lstData[j].Split('|')[2].ToString() != "FEMALE")
                    {
                        lstData[j] = "";
                    }
                }
                #endregion

                #region lọc theo thời gian cập nhập
                if (!ckbAllTime.Checked)
                {
                    for (int j = 0; j < lstData.Count; j++)
                    {
                        if (lstData[j] != "")
                        {
                            timeMess = Convert.ToInt64(lstData[j].Split('|')[4].ToString()) / 1000;
                            if (timeMess < timeStart || timeStart > timeEnd)
                            {
                                lstData[j] = "";
                            }
                        }

                    }
                }

                #endregion

                #region loc dã xem hay chua

                #endregion

                #region lọc theo từ khóa
                string keyword = txtKeyword.Text;
                if (keyword != "")
                {
                    for (int j = 0; j < lstData.Count; j++)
                    {
                        if (lstData[j] != "")
                        {
                            if (!(lstData[j].Split('|')[1].ToString()).Contains(keyword))
                            {
                                lstData[j] = "";
                            }
                        }

                    }
                }
                #endregion

                #region Lọc khách mới
                if (ckbNewCus.Checked)
                {
                    for (int j = 0; j < lstData.Count; j++)
                    {
                        if (lstData[j] != "")
                        {
                            if (lstData[j].Split('|')[5].ToString() == "True")
                            {
                                lstData[j] = "";
                            }
                        }

                    }
                }
                #endregion
                for (int j = 0; j < lstData.Count; j++)
                {
                    if (lstData[j] != "")
                        dgvKhachDaNhanTin.Rows.Add(false, "", lstData[j].Split('|')[0].ToString(), lstData[j].Split('|')[1].ToString(), lstData[j].Split('|')[3].ToString());
                }
            }
            UpdateSTTOnDgv(dgvKhachDaNhanTin, "cSTTUID");
            LoadStatus("Đã load xong danh sách người đã nhắn tin...");
            rControl("stop");
        }

        private void ckbAllTime_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void groupBox6_Enter(object sender, EventArgs e)
        {

        }

        private void ckbAllTime_CheckedChanged_1(object sender, EventArgs e)
        {
            gbTimeFind.Enabled = !ckbAllTime.Checked;
        }
    }
}