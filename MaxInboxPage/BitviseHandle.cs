﻿using ManagedWinapi.Windows;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace maxcare
{
    class BitviseHandle
    {
        #region Windows API - user32.dll configs
        private const int WM_CLOSE = 16;
        private const int BN_CLICKED = 245;
        private const int WM_LBUTTONDOWN = 0x0201;
        private const int WM_LBUTTONUP = 0x0202;

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SendMessage(int hWnd, int msg, int wParam, IntPtr lParam);
        //Click - Worked Perfect
        //SendMessage((int)hwnd, WM_LBUTTONDOWN, 0, IntPtr.Zero);
        //Thread.Sleep(100);
        //SendMessage((int)hwnd, WM_LBUTTONUP, 0, IntPtr.Zero);
        //---
        //Close Window
        //SendMessage((int)hwnd, WM_CLOSE ,0 , IntPtr.Zero);
        #endregion

        private static Hashtable BitviseList = new Hashtable();
        public static int TimeoutSeconds = 30;

        private static int PortIndex = 1079;
        public static int GetPortAvailable()
        {
            /*
            PortIndex++;
            if (PortIndex >= 1280)
            {
                PortIndex = 1079;
            }
            Boolean flag = true;
            while (flag)
            {
                try
                {
                    Process BitviseApp = new Process();
                    BitviseList.Add(PortIndex, BitviseApp);
                    flag = false;
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    PortIndex ++;
                    if (PortIndex >= 1180)
                    {
                        PortIndex = 1079;
                    }
                }
            }
            }
            return PortIndex;
            */
            PortIndex++;
            if (PortIndex >= 1280)
                PortIndex = 1079;
            Process BitviseApp = new Process();
            try
            {
                BitviseList.Add(PortIndex, BitviseApp);
            }
            catch
            {

            }

            return PortIndex;
        }

        public static bool Connect(string Host, string User, string Pass, int ForwardPort)
        {
            bool Connected = false;

            //Start Bitvise - Auto Login
            ProcessStartInfo sinfo = new ProcessStartInfo();


            sinfo.FileName = Base.currentPath + "BitviseSSHClient\\BvSsh.exe";
            sinfo.WorkingDirectory = Base.currentPath + "BitviseSSHClient";
            sinfo.Arguments = "-profile=\"" + Base.currentPath + "BitviseSSHClient\\" + ForwardPort.ToString() + ".bscp\" -host=" + Host + " -user=" + User + " -password=" + Pass + " -loginOnStartup -hide=main,trayIcon,banner,auth,popups,trayLog,trayWRC,trayTerm,traySFTP,trayRDP,trayPopups";
            Process BitviseApp = Process.Start(sinfo);

            BitviseList[ForwardPort] = BitviseApp;

            Thread.Sleep(2000);

            //Bitvise Login Checking...
            for (int i = 0; i < TimeoutSeconds; i++)
            {
                //Detect Host Key Verification
                SystemWindow[] wins = SystemWindow.FilterToplevelWindows((SystemWindow w) => { return w.Title == "Host Key Verification"; });
                if (wins.Length > 0)
                {
                    SystemWindow[] wins2 = wins[0].FilterDescendantWindows(false, (SystemWindow w) => { return w.Title == "&Accept for This Session"; }); //Accept and &Save
                    if (wins2.Length > 0)
                    {
                        //Click 4 times to effected !
                        SendMessage((int)wins2[0].HWnd, WM_LBUTTONDOWN, 0, IntPtr.Zero);
                        Thread.Sleep(10);
                        SendMessage((int)wins2[0].HWnd, WM_LBUTTONUP, 0, IntPtr.Zero);

                        SendMessage((int)wins2[0].HWnd, WM_LBUTTONDOWN, 0, IntPtr.Zero);
                        Thread.Sleep(10);
                        SendMessage((int)wins2[0].HWnd, WM_LBUTTONUP, 0, IntPtr.Zero);
                    }
                }

                //Detect Connected
                SystemWindow[] wins3 = SystemWindow.FilterToplevelWindows((SystemWindow w) => { return w.Title == "Bitvise SSH Client - " + ForwardPort + ".bscp - " + Host + ":22"; });
                if (wins3.Length > 0)
                {
                    Connected = true;
                    break;
                }

                Thread.Sleep(1000);
            }

            if (Connected == false)
            {
                try
                {
                    BitviseApp.Kill();
                    BitviseApp.Dispose();
                }
                catch { }
            }


            return Connected;
        }

        public static void Disconnect(int ForwardPort)
        {
            /*
            if (BitviseList[ForwardPort] == null) return;
            Process BitviseApp = BitviseList[ForwardPort] as Process;
            try
            {
                
                BitviseApp.Kill();
                BitviseApp.Dispose();
            }
            catch(Exception ex) {
                Console.WriteLine(ex.Message);
                BitviseList.Remove(ForwardPort);
            }
            */
            if (BitviseList[ForwardPort] == null)
            {
                return;
            }

            try
            {
                Process BitviseApp = BitviseList[ForwardPort] as Process;
                BitviseApp.Kill();
                BitviseApp.Dispose();

            }
            catch { }
        }

        private static bool GetPort(string Host, int Port)
        {
            return true;
        }

        public static void DisconnectAllBiviseRunning()
        {

            foreach (Process pList in Process.GetProcesses())
            {
                if (pList.ProcessName == "BvSsh")
                {
                    pList.Kill();
                }
            }

            return;

        }

        public static Process FindProcess(IntPtr yourHandle)
        {
            foreach (Process p in Process.GetProcesses())
            {
                if (p.Handle == yourHandle)
                {
                    return p;
                }
            }

            return null;

        }
        }
}
