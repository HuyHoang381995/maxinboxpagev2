﻿namespace maxcare
{
    partial class fMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fMain));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tấtCảToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.liveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkpointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changePasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.đăngNhậpProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cookieToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.uidPassToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.đăngNhậpTrìnhDuyệtMớiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cookieToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.uidPassToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tokenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cookieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uidToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.passToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uidPassToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uidPassTokenCookieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.danhSáchChọnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.danhSáchKhôngChọnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.giữNguyênỞThưMụcCũToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnsCutAccount = new System.Windows.Forms.ToolStripMenuItem();
            this.kiểmTraTàiKhoảnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kiểmTraCookieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kiểmTraTokenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backupTokenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backupCookieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backupCookieTrungGianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mậtKhẩuToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.nhậpDữLiệuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tokenToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tựĐộngLấyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tokenBussinessToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tokenInstagramToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tokenIosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tokenAndroidToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.nhậpDữLiệuToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.cookieToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.tựĐộngLấyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nhậpDữLiệuToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuDragControl2 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.bunifuCards1 = new Bunifu.Framework.UI.BunifuCards();
            this.panel3 = new System.Windows.Forms.Panel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblKey = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel8 = new System.Windows.Forms.ToolStripStatusLabel();
            this.txbUid = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblUser = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel9 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel10 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblDateExpried = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ckbNewCus = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtKeyword = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.lblTu = new System.Windows.Forms.Label();
            this.lblDen = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rbNu = new System.Windows.Forms.RadioButton();
            this.rbNam = new System.Windows.Forms.RadioButton();
            this.rbAllGender = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.btnQuyetInbox = new System.Windows.Forms.Button();
            this.btnCauHinh = new System.Windows.Forms.Button();
            this.lblTrangthai = new System.Windows.Forms.Label();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.btnPause = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dgvKhachDaNhanTin = new System.Windows.Forms.DataGridView();
            this.cChoseUID = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cSTTUID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cUID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cIdPageOfCus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ctmsCustomer = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.chọnBôiĐenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chọnTâtCảToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bỏChọnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bỏChọnTấtCảToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvPage = new System.Windows.Forms.DataGridView();
            this.cChosePage = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cSTT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cIdPage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cTenPage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cLike = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cTokenPage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ctmsPage = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.chọnTấtCảToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bỏChọnTấtCảToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chonBôiĐenToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.xóaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadNgườiĐãNhắnTinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtCookie = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ckbAllTime = new System.Windows.Forms.CheckBox();
            this.gbTimeFind = new System.Windows.Forms.GroupBox();
            this.pnlHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.bunifuCards1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKhachDaNhanTin)).BeginInit();
            this.ctmsCustomer.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPage)).BeginInit();
            this.ctmsPage.SuspendLayout();
            this.gbTimeFind.SuspendLayout();
            this.SuspendLayout();
            // 
            // tấtCảToolStripMenuItem
            // 
            this.tấtCảToolStripMenuItem.Name = "tấtCảToolStripMenuItem";
            this.tấtCảToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // liveToolStripMenuItem
            // 
            this.liveToolStripMenuItem.Name = "liveToolStripMenuItem";
            this.liveToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // dieToolStripMenuItem
            // 
            this.dieToolStripMenuItem.Name = "dieToolStripMenuItem";
            this.dieToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // checkpointToolStripMenuItem
            // 
            this.checkpointToolStripMenuItem.Name = "checkpointToolStripMenuItem";
            this.checkpointToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // changePasswordToolStripMenuItem
            // 
            this.changePasswordToolStripMenuItem.Name = "changePasswordToolStripMenuItem";
            this.changePasswordToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // đăngNhậpProfileToolStripMenuItem
            // 
            this.đăngNhậpProfileToolStripMenuItem.Name = "đăngNhậpProfileToolStripMenuItem";
            this.đăngNhậpProfileToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // cookieToolStripMenuItem1
            // 
            this.cookieToolStripMenuItem1.Name = "cookieToolStripMenuItem1";
            this.cookieToolStripMenuItem1.Size = new System.Drawing.Size(32, 19);
            // 
            // uidPassToolStripMenuItem1
            // 
            this.uidPassToolStripMenuItem1.Name = "uidPassToolStripMenuItem1";
            this.uidPassToolStripMenuItem1.Size = new System.Drawing.Size(32, 19);
            // 
            // đăngNhậpTrìnhDuyệtMớiToolStripMenuItem
            // 
            this.đăngNhậpTrìnhDuyệtMớiToolStripMenuItem.Name = "đăngNhậpTrìnhDuyệtMớiToolStripMenuItem";
            this.đăngNhậpTrìnhDuyệtMớiToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // cookieToolStripMenuItem2
            // 
            this.cookieToolStripMenuItem2.Name = "cookieToolStripMenuItem2";
            this.cookieToolStripMenuItem2.Size = new System.Drawing.Size(32, 19);
            // 
            // uidPassToolStripMenuItem2
            // 
            this.uidPassToolStripMenuItem2.Name = "uidPassToolStripMenuItem2";
            this.uidPassToolStripMenuItem2.Size = new System.Drawing.Size(32, 19);
            // 
            // tokenToolStripMenuItem
            // 
            this.tokenToolStripMenuItem.Name = "tokenToolStripMenuItem";
            this.tokenToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // cookieToolStripMenuItem
            // 
            this.cookieToolStripMenuItem.Name = "cookieToolStripMenuItem";
            this.cookieToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // uidToolStripMenuItem
            // 
            this.uidToolStripMenuItem.Name = "uidToolStripMenuItem";
            this.uidToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // passToolStripMenuItem
            // 
            this.passToolStripMenuItem.Name = "passToolStripMenuItem";
            this.passToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // uidPassToolStripMenuItem
            // 
            this.uidPassToolStripMenuItem.Name = "uidPassToolStripMenuItem";
            this.uidPassToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // uidPassTokenCookieToolStripMenuItem
            // 
            this.uidPassTokenCookieToolStripMenuItem.Name = "uidPassTokenCookieToolStripMenuItem";
            this.uidPassTokenCookieToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // danhSáchChọnToolStripMenuItem
            // 
            this.danhSáchChọnToolStripMenuItem.Name = "danhSáchChọnToolStripMenuItem";
            this.danhSáchChọnToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // danhSáchKhôngChọnToolStripMenuItem
            // 
            this.danhSáchKhôngChọnToolStripMenuItem.Name = "danhSáchKhôngChọnToolStripMenuItem";
            this.danhSáchKhôngChọnToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // giữNguyênỞThưMụcCũToolStripMenuItem
            // 
            this.giữNguyênỞThưMụcCũToolStripMenuItem.Name = "giữNguyênỞThưMụcCũToolStripMenuItem";
            this.giữNguyênỞThưMụcCũToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // mnsCutAccount
            // 
            this.mnsCutAccount.Name = "mnsCutAccount";
            this.mnsCutAccount.Size = new System.Drawing.Size(32, 19);
            // 
            // kiểmTraTàiKhoảnToolStripMenuItem
            // 
            this.kiểmTraTàiKhoảnToolStripMenuItem.Name = "kiểmTraTàiKhoảnToolStripMenuItem";
            this.kiểmTraTàiKhoảnToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // kiểmTraCookieToolStripMenuItem
            // 
            this.kiểmTraCookieToolStripMenuItem.Name = "kiểmTraCookieToolStripMenuItem";
            this.kiểmTraCookieToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // kiểmTraTokenToolStripMenuItem
            // 
            this.kiểmTraTokenToolStripMenuItem.Name = "kiểmTraTokenToolStripMenuItem";
            this.kiểmTraTokenToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // backupTokenToolStripMenuItem
            // 
            this.backupTokenToolStripMenuItem.Name = "backupTokenToolStripMenuItem";
            this.backupTokenToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // backupCookieToolStripMenuItem
            // 
            this.backupCookieToolStripMenuItem.Name = "backupCookieToolStripMenuItem";
            this.backupCookieToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // backupCookieTrungGianToolStripMenuItem
            // 
            this.backupCookieTrungGianToolStripMenuItem.Name = "backupCookieTrungGianToolStripMenuItem";
            this.backupCookieTrungGianToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // mậtKhẩuToolStripMenuItem1
            // 
            this.mậtKhẩuToolStripMenuItem1.Name = "mậtKhẩuToolStripMenuItem1";
            this.mậtKhẩuToolStripMenuItem1.Size = new System.Drawing.Size(32, 19);
            // 
            // nhậpDữLiệuToolStripMenuItem
            // 
            this.nhậpDữLiệuToolStripMenuItem.Name = "nhậpDữLiệuToolStripMenuItem";
            this.nhậpDữLiệuToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // tokenToolStripMenuItem2
            // 
            this.tokenToolStripMenuItem2.Name = "tokenToolStripMenuItem2";
            this.tokenToolStripMenuItem2.Size = new System.Drawing.Size(32, 19);
            // 
            // tựĐộngLấyToolStripMenuItem1
            // 
            this.tựĐộngLấyToolStripMenuItem1.Name = "tựĐộngLấyToolStripMenuItem1";
            this.tựĐộngLấyToolStripMenuItem1.Size = new System.Drawing.Size(32, 19);
            // 
            // tokenBussinessToolStripMenuItem
            // 
            this.tokenBussinessToolStripMenuItem.Name = "tokenBussinessToolStripMenuItem";
            this.tokenBussinessToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // tokenInstagramToolStripMenuItem1
            // 
            this.tokenInstagramToolStripMenuItem1.Name = "tokenInstagramToolStripMenuItem1";
            this.tokenInstagramToolStripMenuItem1.Size = new System.Drawing.Size(32, 19);
            // 
            // tokenIosToolStripMenuItem
            // 
            this.tokenIosToolStripMenuItem.Name = "tokenIosToolStripMenuItem";
            this.tokenIosToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // tokenAndroidToolStripMenuItem1
            // 
            this.tokenAndroidToolStripMenuItem1.Name = "tokenAndroidToolStripMenuItem1";
            this.tokenAndroidToolStripMenuItem1.Size = new System.Drawing.Size(32, 19);
            // 
            // nhậpDữLiệuToolStripMenuItem2
            // 
            this.nhậpDữLiệuToolStripMenuItem2.Name = "nhậpDữLiệuToolStripMenuItem2";
            this.nhậpDữLiệuToolStripMenuItem2.Size = new System.Drawing.Size(32, 19);
            // 
            // cookieToolStripMenuItem3
            // 
            this.cookieToolStripMenuItem3.Name = "cookieToolStripMenuItem3";
            this.cookieToolStripMenuItem3.Size = new System.Drawing.Size(32, 19);
            // 
            // tựĐộngLấyToolStripMenuItem
            // 
            this.tựĐộngLấyToolStripMenuItem.Name = "tựĐộngLấyToolStripMenuItem";
            this.tựĐộngLấyToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // nhậpDữLiệuToolStripMenuItem1
            // 
            this.nhậpDữLiệuToolStripMenuItem1.Name = "nhậpDữLiệuToolStripMenuItem1";
            this.nhậpDữLiệuToolStripMenuItem1.Size = new System.Drawing.Size(32, 19);
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.pnlHeader;
            this.bunifuDragControl1.Vertical = true;
            // 
            // pnlHeader
            // 
            this.pnlHeader.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlHeader.BackColor = System.Drawing.Color.White;
            this.pnlHeader.Controls.Add(this.pictureBox1);
            this.pnlHeader.Controls.Add(this.button2);
            this.pnlHeader.Controls.Add(this.button1);
            this.pnlHeader.Controls.Add(this.btnClose);
            this.pnlHeader.Controls.Add(this.bunifuCustomLabel1);
            this.pnlHeader.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.pnlHeader.Location = new System.Drawing.Point(1, 5);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(1083, 32);
            this.pnlHeader.TabIndex = 9;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(5, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(34, 27);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(984, -2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(32, 32);
            this.button2.TabIndex = 15;
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Enabled = false;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(1016, -2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(32, 32);
            this.button1.TabIndex = 14;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(1048, -2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(32, 32);
            this.btnClose.TabIndex = 13;
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.ForeColor = System.Drawing.Color.Black;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(41, 8);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(482, 16);
            this.bunifuCustomLabel1.TabIndex = 7;
            this.bunifuCustomLabel1.Text = "Max Inbox Page - Phần Mềm Tự Động Rep Nhắn Tin Page - MIN SOFTWARE";
            // 
            // bunifuDragControl2
            // 
            this.bunifuDragControl2.Fixed = true;
            this.bunifuDragControl2.Horizontal = true;
            this.bunifuDragControl2.TargetControl = null;
            this.bunifuDragControl2.Vertical = true;
            // 
            // bunifuCards1
            // 
            this.bunifuCards1.BackColor = System.Drawing.Color.White;
            this.bunifuCards1.BorderRadius = 0;
            this.bunifuCards1.BottomSahddow = true;
            this.bunifuCards1.color = System.Drawing.Color.RoyalBlue;
            this.bunifuCards1.Controls.Add(this.pnlHeader);
            this.bunifuCards1.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuCards1.LeftSahddow = false;
            this.bunifuCards1.Location = new System.Drawing.Point(0, 0);
            this.bunifuCards1.Name = "bunifuCards1";
            this.bunifuCards1.RightSahddow = true;
            this.bunifuCards1.ShadowDepth = 20;
            this.bunifuCards1.Size = new System.Drawing.Size(1083, 38);
            this.bunifuCards1.TabIndex = 10;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.Controls.Add(this.statusStrip1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 543);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1083, 26);
            this.panel3.TabIndex = 11;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.lblStatus,
            this.toolStripStatusLabel3,
            this.lblKey,
            this.toolStripStatusLabel8,
            this.txbUid,
            this.lblUser,
            this.toolStripStatusLabel9,
            this.toolStripStatusLabel10,
            this.lblDateExpried});
            this.statusStrip1.Location = new System.Drawing.Point(0, 4);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1083, 22);
            this.statusStrip1.TabIndex = 13;
            this.statusStrip1.Text = "statusStrip1";
            this.statusStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusStrip1_ItemClicked);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(79, 17);
            this.toolStripStatusLabel1.Text = "Trạng thái:";
            // 
            // lblStatus
            // 
            this.lblStatus.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.Color.Green;
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(97, 17);
            this.lblStatus.Text = "Đang kiểm tra...";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(82, 17);
            this.toolStripStatusLabel3.Text = "Mã thiết bị:";
            // 
            // lblKey
            // 
            this.lblKey.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKey.ForeColor = System.Drawing.Color.Teal;
            this.lblKey.Name = "lblKey";
            this.lblKey.Size = new System.Drawing.Size(49, 17);
            this.lblKey.Text = "******";
            // 
            // toolStripStatusLabel8
            // 
            this.toolStripStatusLabel8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabel8.Name = "toolStripStatusLabel8";
            this.toolStripStatusLabel8.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel8.Text = "User:";
            // 
            // txbUid
            // 
            this.txbUid.Name = "txbUid";
            this.txbUid.Size = new System.Drawing.Size(0, 17);
            // 
            // lblUser
            // 
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(56, 17);
            this.lblUser.Text = "******";
            // 
            // toolStripStatusLabel9
            // 
            this.toolStripStatusLabel9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabel9.IsLink = true;
            this.toolStripStatusLabel9.Name = "toolStripStatusLabel9";
            this.toolStripStatusLabel9.Size = new System.Drawing.Size(66, 17);
            this.toolStripStatusLabel9.Text = "Đăng xuất";
            this.toolStripStatusLabel9.Click += new System.EventHandler(this.toolStripStatusLabel9_Click);
            // 
            // toolStripStatusLabel10
            // 
            this.toolStripStatusLabel10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabel10.Name = "toolStripStatusLabel10";
            this.toolStripStatusLabel10.Size = new System.Drawing.Size(99, 17);
            this.toolStripStatusLabel10.Text = "Ngày hết hạn:";
            // 
            // lblDateExpried
            // 
            this.lblDateExpried.Name = "lblDateExpried";
            this.lblDateExpried.Size = new System.Drawing.Size(74, 17);
            this.lblDateExpried.Text = "20/10/2020";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.btnCauHinh);
            this.groupBox2.Controls.Add(this.lblTrangthai);
            this.groupBox2.Controls.Add(this.metroButton1);
            this.groupBox2.Controls.Add(this.btnPause);
            this.groupBox2.Controls.Add(this.btnStart);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.txtCookie);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.groupBox2.Location = new System.Drawing.Point(4, 41);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1077, 496);
            this.groupBox2.TabIndex = 68;
            this.groupBox2.TabStop = false;
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ckbNewCus);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.txtKeyword);
            this.groupBox4.Controls.Add(this.groupBox6);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.btnQuyetInbox);
            this.groupBox4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.groupBox4.Location = new System.Drawing.Point(9, 311);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(562, 179);
            this.groupBox4.TabIndex = 166;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Khoảng thời gian";
            // 
            // ckbNewCus
            // 
            this.ckbNewCus.AutoSize = true;
            this.ckbNewCus.Location = new System.Drawing.Point(401, 78);
            this.ckbNewCus.Name = "ckbNewCus";
            this.ckbNewCus.Size = new System.Drawing.Size(118, 20);
            this.ckbNewCus.TabIndex = 83;
            this.ckbNewCus.Text = "Khách hàng mới";
            this.ckbNewCus.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(398, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 16);
            this.label5.TabIndex = 82;
            this.label5.Text = "Từ khóa:";
            // 
            // txtKeyword
            // 
            this.txtKeyword.Location = new System.Drawing.Point(401, 49);
            this.txtKeyword.Name = "txtKeyword";
            this.txtKeyword.Size = new System.Drawing.Size(155, 23);
            this.txtKeyword.TabIndex = 81;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.gbTimeFind);
            this.groupBox6.Controls.Add(this.ckbAllTime);
            this.groupBox6.Location = new System.Drawing.Point(12, 19);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(171, 154);
            this.groupBox6.TabIndex = 79;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Thời gian";
            this.groupBox6.Enter += new System.EventHandler(this.groupBox6_Enter);
            // 
            // dtpStart
            // 
            this.dtpStart.CustomFormat = "dd/MM/yyyy";
            this.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStart.Location = new System.Drawing.Point(14, 27);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Size = new System.Drawing.Size(107, 23);
            this.dtpStart.TabIndex = 75;
            // 
            // dtpEnd
            // 
            this.dtpEnd.CustomFormat = "dd/MM/yyyy";
            this.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEnd.Location = new System.Drawing.Point(14, 72);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(107, 23);
            this.dtpEnd.TabIndex = 76;
            // 
            // lblTu
            // 
            this.lblTu.AutoSize = true;
            this.lblTu.Location = new System.Drawing.Point(11, 9);
            this.lblTu.Name = "lblTu";
            this.lblTu.Size = new System.Drawing.Size(29, 16);
            this.lblTu.TabIndex = 73;
            this.lblTu.Text = "Từ:";
            // 
            // lblDen
            // 
            this.lblDen.AutoSize = true;
            this.lblDen.Location = new System.Drawing.Point(11, 53);
            this.lblDen.Name = "lblDen";
            this.lblDen.Size = new System.Drawing.Size(34, 16);
            this.lblDen.TabIndex = 74;
            this.lblDen.Text = "đến:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.rbNu);
            this.groupBox5.Controls.Add(this.rbNam);
            this.groupBox5.Controls.Add(this.rbAllGender);
            this.groupBox5.Location = new System.Drawing.Point(189, 19);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(206, 77);
            this.groupBox5.TabIndex = 78;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Giới tính";
            // 
            // rbNu
            // 
            this.rbNu.AutoSize = true;
            this.rbNu.Location = new System.Drawing.Point(132, 33);
            this.rbNu.Name = "rbNu";
            this.rbNu.Size = new System.Drawing.Size(42, 20);
            this.rbNu.TabIndex = 2;
            this.rbNu.TabStop = true;
            this.rbNu.Text = "Nữ";
            this.rbNu.UseVisualStyleBackColor = true;
            // 
            // rbNam
            // 
            this.rbNam.AutoSize = true;
            this.rbNam.Location = new System.Drawing.Point(75, 33);
            this.rbNam.Name = "rbNam";
            this.rbNam.Size = new System.Drawing.Size(52, 20);
            this.rbNam.TabIndex = 1;
            this.rbNam.TabStop = true;
            this.rbNam.Text = "Nam";
            this.rbNam.UseVisualStyleBackColor = true;
            // 
            // rbAllGender
            // 
            this.rbAllGender.AutoSize = true;
            this.rbAllGender.Location = new System.Drawing.Point(7, 33);
            this.rbAllGender.Name = "rbAllGender";
            this.rbAllGender.Size = new System.Drawing.Size(62, 20);
            this.rbAllGender.TabIndex = 0;
            this.rbAllGender.TabStop = true;
            this.rbAllGender.Text = "Tất cả";
            this.rbAllGender.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 16);
            this.label1.TabIndex = 72;
            // 
            // btnQuyetInbox
            // 
            this.btnQuyetInbox.BackColor = System.Drawing.Color.White;
            this.btnQuyetInbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnQuyetInbox.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnQuyetInbox.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnQuyetInbox.Image = global::maxcare.Properties.Resources.icons8_search_23px1;
            this.btnQuyetInbox.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnQuyetInbox.Location = new System.Drawing.Point(305, 120);
            this.btnQuyetInbox.Name = "btnQuyetInbox";
            this.btnQuyetInbox.Size = new System.Drawing.Size(129, 40);
            this.btnQuyetInbox.TabIndex = 71;
            this.btnQuyetInbox.Text = "Quyét";
            this.btnQuyetInbox.UseVisualStyleBackColor = false;
            this.btnQuyetInbox.Click += new System.EventHandler(this.btnQuyetInbox_Click);
            // 
            // btnCauHinh
            // 
            this.btnCauHinh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCauHinh.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnCauHinh.Image = global::maxcare.Properties.Resources.icons8_services_30px1;
            this.btnCauHinh.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCauHinh.Location = new System.Drawing.Point(17, 58);
            this.btnCauHinh.Name = "btnCauHinh";
            this.btnCauHinh.Size = new System.Drawing.Size(109, 40);
            this.btnCauHinh.TabIndex = 165;
            this.btnCauHinh.TabStop = false;
            this.btnCauHinh.Text = "Cấu hình";
            this.btnCauHinh.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCauHinh.UseVisualStyleBackColor = false;
            this.btnCauHinh.Click += new System.EventHandler(this.btnCauHinh_Click);
            // 
            // lblTrangthai
            // 
            this.lblTrangthai.AutoSize = true;
            this.lblTrangthai.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.lblTrangthai.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangthai.ForeColor = System.Drawing.Color.Green;
            this.lblTrangthai.Location = new System.Drawing.Point(388, 70);
            this.lblTrangthai.Name = "lblTrangthai";
            this.lblTrangthai.Size = new System.Drawing.Size(0, 16);
            this.lblTrangthai.TabIndex = 163;
            // 
            // metroButton1
            // 
            this.metroButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.metroButton1.Location = new System.Drawing.Point(874, 20);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(139, 27);
            this.metroButton1.TabIndex = 164;
            this.metroButton1.Text = "Load danh sách Page";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.Button3_Click);
            // 
            // btnPause
            // 
            this.btnPause.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPause.Enabled = false;
            this.btnPause.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnPause.Image = ((System.Drawing.Image)(resources.GetObject("btnPause.Image")));
            this.btnPause.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPause.Location = new System.Drawing.Point(247, 58);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(109, 40);
            this.btnPause.TabIndex = 70;
            this.btnPause.Text = "Tạm dừng";
            this.btnPause.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPause.UseVisualStyleBackColor = false;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click_1);
            // 
            // btnStart
            // 
            this.btnStart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStart.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnStart.Image = ((System.Drawing.Image)(resources.GetObject("btnStart.Image")));
            this.btnStart.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnStart.Location = new System.Drawing.Point(132, 58);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(109, 40);
            this.btnStart.TabIndex = 69;
            this.btnStart.Text = "Bắt đầu";
            this.btnStart.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnShare_Click_1);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dgvKhachDaNhanTin);
            this.groupBox3.Location = new System.Drawing.Point(577, 104);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(487, 386);
            this.groupBox3.TabIndex = 77;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Danh sách người nhắn tin page";
            // 
            // dgvKhachDaNhanTin
            // 
            this.dgvKhachDaNhanTin.AllowUserToAddRows = false;
            this.dgvKhachDaNhanTin.AllowUserToDeleteRows = false;
            this.dgvKhachDaNhanTin.AllowUserToResizeRows = false;
            this.dgvKhachDaNhanTin.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 9.75F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvKhachDaNhanTin.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvKhachDaNhanTin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKhachDaNhanTin.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cChoseUID,
            this.cSTTUID,
            this.cUID,
            this.dataGridViewTextBoxColumn3,
            this.cIdPageOfCus,
            this.cState});
            this.dgvKhachDaNhanTin.ContextMenuStrip = this.ctmsCustomer;
            this.dgvKhachDaNhanTin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvKhachDaNhanTin.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvKhachDaNhanTin.Location = new System.Drawing.Point(3, 19);
            this.dgvKhachDaNhanTin.Name = "dgvKhachDaNhanTin";
            this.dgvKhachDaNhanTin.RowHeadersVisible = false;
            this.dgvKhachDaNhanTin.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvKhachDaNhanTin.Size = new System.Drawing.Size(481, 364);
            this.dgvKhachDaNhanTin.TabIndex = 76;
            this.dgvKhachDaNhanTin.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvKhachDaNhanTin_CellClick);
            // 
            // cChoseUID
            // 
            this.cChoseUID.HeaderText = "Chọn";
            this.cChoseUID.Name = "cChoseUID";
            this.cChoseUID.Width = 40;
            // 
            // cSTTUID
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.cSTTUID.DefaultCellStyle = dataGridViewCellStyle6;
            this.cSTTUID.HeaderText = "STT";
            this.cSTTUID.Name = "cSTTUID";
            this.cSTTUID.Width = 40;
            // 
            // cUID
            // 
            this.cUID.HeaderText = "UID";
            this.cUID.Name = "cUID";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.HeaderText = "Tên";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // cIdPageOfCus
            // 
            this.cIdPageOfCus.HeaderText = "ID Page";
            this.cIdPageOfCus.Name = "cIdPageOfCus";
            this.cIdPageOfCus.Visible = false;
            // 
            // cState
            // 
            this.cState.HeaderText = "Trạng thái";
            this.cState.Name = "cState";
            // 
            // ctmsCustomer
            // 
            this.ctmsCustomer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chọnBôiĐenToolStripMenuItem,
            this.chọnTâtCảToolStripMenuItem,
            this.bỏChọnToolStripMenuItem,
            this.bỏChọnTấtCảToolStripMenuItem1});
            this.ctmsCustomer.Name = "ctmsCustomer";
            this.ctmsCustomer.Size = new System.Drawing.Size(151, 92);
            // 
            // chọnBôiĐenToolStripMenuItem
            // 
            this.chọnBôiĐenToolStripMenuItem.Name = "chọnBôiĐenToolStripMenuItem";
            this.chọnBôiĐenToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.chọnBôiĐenToolStripMenuItem.Text = "Chọn bôi đen";
            this.chọnBôiĐenToolStripMenuItem.Click += new System.EventHandler(this.chọnBôiĐenToolStripMenuItem_Click);
            // 
            // chọnTâtCảToolStripMenuItem
            // 
            this.chọnTâtCảToolStripMenuItem.Name = "chọnTâtCảToolStripMenuItem";
            this.chọnTâtCảToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.chọnTâtCảToolStripMenuItem.Text = "Chọn tât cả";
            this.chọnTâtCảToolStripMenuItem.Click += new System.EventHandler(this.chọnTâtCảToolStripMenuItem_Click);
            // 
            // bỏChọnToolStripMenuItem
            // 
            this.bỏChọnToolStripMenuItem.Name = "bỏChọnToolStripMenuItem";
            this.bỏChọnToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.bỏChọnToolStripMenuItem.Text = "Bỏ chọn";
            this.bỏChọnToolStripMenuItem.Click += new System.EventHandler(this.bỏChọnToolStripMenuItem_Click);
            // 
            // bỏChọnTấtCảToolStripMenuItem1
            // 
            this.bỏChọnTấtCảToolStripMenuItem1.Name = "bỏChọnTấtCảToolStripMenuItem1";
            this.bỏChọnTấtCảToolStripMenuItem1.Size = new System.Drawing.Size(150, 22);
            this.bỏChọnTấtCảToolStripMenuItem1.Text = "Bỏ chọn tất cả";
            this.bỏChọnTấtCảToolStripMenuItem1.Click += new System.EventHandler(this.bỏChọnTấtCảToolStripMenuItem1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvPage);
            this.groupBox1.Location = new System.Drawing.Point(8, 104);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(566, 203);
            this.groupBox1.TabIndex = 76;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Danh sách Page";
            // 
            // dgvPage
            // 
            this.dgvPage.AllowUserToAddRows = false;
            this.dgvPage.AllowUserToDeleteRows = false;
            this.dgvPage.AllowUserToResizeRows = false;
            this.dgvPage.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Teal;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPage.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPage.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cChosePage,
            this.cSTT,
            this.cIdPage,
            this.cTenPage,
            this.cLike,
            this.cTokenPage});
            this.dgvPage.ContextMenuStrip = this.ctmsPage;
            this.dgvPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPage.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvPage.Location = new System.Drawing.Point(3, 19);
            this.dgvPage.Name = "dgvPage";
            this.dgvPage.RowHeadersVisible = false;
            this.dgvPage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPage.Size = new System.Drawing.Size(560, 181);
            this.dgvPage.TabIndex = 75;
            this.dgvPage.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgvAcc_CellClick);
            this.dgvPage.SortCompare += new System.Windows.Forms.DataGridViewSortCompareEventHandler(this.dgvPage_SortCompare);
            this.dgvPage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtgvAcc_KeyDown);
            // 
            // cChosePage
            // 
            this.cChosePage.HeaderText = "Chọn";
            this.cChosePage.Name = "cChosePage";
            this.cChosePage.Width = 40;
            // 
            // cSTT
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.cSTT.DefaultCellStyle = dataGridViewCellStyle7;
            this.cSTT.HeaderText = "STT";
            this.cSTT.Name = "cSTT";
            this.cSTT.Width = 40;
            // 
            // cIdPage
            // 
            this.cIdPage.HeaderText = "Id Page";
            this.cIdPage.Name = "cIdPage";
            // 
            // cTenPage
            // 
            this.cTenPage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cTenPage.HeaderText = "Tên page";
            this.cTenPage.Name = "cTenPage";
            // 
            // cLike
            // 
            this.cLike.HeaderText = "Like";
            this.cLike.Name = "cLike";
            this.cLike.Width = 70;
            // 
            // cTokenPage
            // 
            this.cTokenPage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.cTokenPage.HeaderText = "Token Page";
            this.cTokenPage.Name = "cTokenPage";
            this.cTokenPage.Visible = false;
            // 
            // ctmsPage
            // 
            this.ctmsPage.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctmsPage.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chọnTấtCảToolStripMenuItem,
            this.bỏChọnTấtCảToolStripMenuItem,
            this.chonBôiĐenToolStripMenuItem1,
            this.xóaToolStripMenuItem,
            this.loadNgườiĐãNhắnTinToolStripMenuItem});
            this.ctmsPage.Name = "ctmsAcc";
            this.ctmsPage.Size = new System.Drawing.Size(202, 114);
            // 
            // chọnTấtCảToolStripMenuItem
            // 
            this.chọnTấtCảToolStripMenuItem.Name = "chọnTấtCảToolStripMenuItem";
            this.chọnTấtCảToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.chọnTấtCảToolStripMenuItem.Text = "Chọn tất cả";
            this.chọnTấtCảToolStripMenuItem.Click += new System.EventHandler(this.chọnTấtCảToolStripMenuItem_Click_1);
            // 
            // bỏChọnTấtCảToolStripMenuItem
            // 
            this.bỏChọnTấtCảToolStripMenuItem.Name = "bỏChọnTấtCảToolStripMenuItem";
            this.bỏChọnTấtCảToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.bỏChọnTấtCảToolStripMenuItem.Text = "Bỏ chọn tất cả";
            this.bỏChọnTấtCảToolStripMenuItem.Click += new System.EventHandler(this.bỏChọnTấtCảToolStripMenuItem_Click_1);
            // 
            // chonBôiĐenToolStripMenuItem1
            // 
            this.chonBôiĐenToolStripMenuItem1.Name = "chonBôiĐenToolStripMenuItem1";
            this.chonBôiĐenToolStripMenuItem1.Size = new System.Drawing.Size(201, 22);
            this.chonBôiĐenToolStripMenuItem1.Text = "Chọn bôi đen";
            this.chonBôiĐenToolStripMenuItem1.Click += new System.EventHandler(this.chonBôiĐenToolStripMenuItem1_Click);
            // 
            // xóaToolStripMenuItem
            // 
            this.xóaToolStripMenuItem.Name = "xóaToolStripMenuItem";
            this.xóaToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.xóaToolStripMenuItem.Text = "Xóa đã chọn";
            this.xóaToolStripMenuItem.Click += new System.EventHandler(this.xóaToolStripMenuItem_Click_1);
            // 
            // loadNgườiĐãNhắnTinToolStripMenuItem
            // 
            this.loadNgườiĐãNhắnTinToolStripMenuItem.Name = "loadNgườiĐãNhắnTinToolStripMenuItem";
            this.loadNgườiĐãNhắnTinToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.loadNgườiĐãNhắnTinToolStripMenuItem.Text = "Load người đã nhắn tin";
            this.loadNgườiĐãNhắnTinToolStripMenuItem.Visible = false;
            this.loadNgườiĐãNhắnTinToolStripMenuItem.Click += new System.EventHandler(this.loadNgườiĐãNhắnTinToolStripMenuItem_Click);
            // 
            // txtCookie
            // 
            this.txtCookie.Location = new System.Drawing.Point(72, 24);
            this.txtCookie.Name = "txtCookie";
            this.txtCookie.Size = new System.Drawing.Size(785, 23);
            this.txtCookie.TabIndex = 149;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 16);
            this.label3.TabIndex = 148;
            this.label3.Text = "Cookie:";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // ckbAllTime
            // 
            this.ckbAllTime.AutoSize = true;
            this.ckbAllTime.Location = new System.Drawing.Point(6, 119);
            this.ckbAllTime.Name = "ckbAllTime";
            this.ckbAllTime.Size = new System.Drawing.Size(63, 20);
            this.ckbAllTime.TabIndex = 84;
            this.ckbAllTime.Text = "Tất cả";
            this.ckbAllTime.UseVisualStyleBackColor = true;
            this.ckbAllTime.CheckedChanged += new System.EventHandler(this.ckbAllTime_CheckedChanged_1);
            // 
            // gbTimeFind
            // 
            this.gbTimeFind.Controls.Add(this.dtpStart);
            this.gbTimeFind.Controls.Add(this.lblDen);
            this.gbTimeFind.Controls.Add(this.dtpEnd);
            this.gbTimeFind.Controls.Add(this.lblTu);
            this.gbTimeFind.Location = new System.Drawing.Point(6, 18);
            this.gbTimeFind.Name = "gbTimeFind";
            this.gbTimeFind.Size = new System.Drawing.Size(159, 100);
            this.gbTimeFind.TabIndex = 85;
            this.gbTimeFind.TabStop = false;
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1083, 569);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.bunifuCards1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "fMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Max Invite Like Page v2";
            this.Load += new System.EventHandler(this.FMain_Load);
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.bunifuCards1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvKhachDaNhanTin)).EndInit();
            this.ctmsCustomer.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPage)).EndInit();
            this.ctmsPage.ResumeLayout(false);
            this.gbTimeFind.ResumeLayout(false);
            this.gbTimeFind.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl2;
        private Bunifu.Framework.UI.BunifuCards bunifuCards1;
        private System.Windows.Forms.Panel pnlHeader;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ToolStripMenuItem liveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkpointToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changePasswordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tokenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cookieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uidPassToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uidPassTokenCookieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem danhSáchChọnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem danhSáchKhôngChọnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tấtCảToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uidToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem passToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem giữNguyênỞThưMụcCũToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnsCutAccount;
        private System.Windows.Forms.ToolStripMenuItem đăngNhậpProfileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cookieToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem uidPassToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem đăngNhậpTrìnhDuyệtMớiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cookieToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem uidPassToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem kiểmTraCookieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kiểmTraTokenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kiểmTraTàiKhoảnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mậtKhẩuToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem nhậpDữLiệuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tokenToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem tựĐộngLấyToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tokenBussinessToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tokenInstagramToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tokenIosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tokenAndroidToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem nhậpDữLiệuToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem cookieToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem tựĐộngLấyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nhậpDữLiệuToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem backupTokenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backupCookieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backupCookieTrungGianToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.DataGridView dgvPage;
        private System.Windows.Forms.ContextMenuStrip ctmsPage;
        private System.Windows.Forms.ToolStripMenuItem chọnTấtCảToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bỏChọnTấtCảToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xóaToolStripMenuItem;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel lblKey;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel8;
        private System.Windows.Forms.ToolStripStatusLabel txbUid;
        private System.Windows.Forms.ToolStripStatusLabel lblUser;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel9;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel10;
        private System.Windows.Forms.ToolStripStatusLabel lblDateExpried;
        private System.Windows.Forms.TextBox txtCookie;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblTrangthai;
        private System.Windows.Forms.ToolStripMenuItem chonBôiĐenToolStripMenuItem1;
        private MetroFramework.Controls.MetroButton metroButton1;
        private System.Windows.Forms.Button btnCauHinh;
        public System.Windows.Forms.DataGridView dgvKhachDaNhanTin;
        private System.Windows.Forms.ToolStripMenuItem loadNgườiĐãNhắnTinToolStripMenuItem;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cChoseUID;
        private System.Windows.Forms.DataGridViewTextBoxColumn cSTTUID;
        private System.Windows.Forms.DataGridViewTextBoxColumn cUID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn cIdPageOfCus;
        private System.Windows.Forms.ContextMenuStrip ctmsCustomer;
        private System.Windows.Forms.ToolStripMenuItem chọnBôiĐenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chọnTâtCảToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bỏChọnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bỏChọnTấtCảToolStripMenuItem1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cChosePage;
        private System.Windows.Forms.DataGridViewTextBoxColumn cSTT;
        private System.Windows.Forms.DataGridViewTextBoxColumn cIdPage;
        private System.Windows.Forms.DataGridViewTextBoxColumn cTenPage;
        private System.Windows.Forms.DataGridViewTextBoxColumn cLike;
        private System.Windows.Forms.DataGridViewTextBoxColumn cTokenPage;
        private System.Windows.Forms.DataGridViewTextBoxColumn cState;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DateTimePicker dtpEnd;
        private System.Windows.Forms.DateTimePicker dtpStart;
        private System.Windows.Forms.Label lblDen;
        private System.Windows.Forms.Label lblTu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnQuyetInbox;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox ckbNewCus;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtKeyword;
        private System.Windows.Forms.RadioButton rbNu;
        private System.Windows.Forms.RadioButton rbNam;
        private System.Windows.Forms.RadioButton rbAllGender;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.CheckBox ckbAllTime;
        private System.Windows.Forms.GroupBox gbTimeFind;
    }
}

